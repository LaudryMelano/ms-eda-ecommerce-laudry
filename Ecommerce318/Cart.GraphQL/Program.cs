using Cart.Domain;
using Cart.Domain.MapProfile;
using Cart.Domain.Repositories;
using Cart.Domain.Services;
using Cart.GraphQL.Schema.Mutations;
using Cart.GraphQL.Schema.Querires;
using Framework.Core.Events;
using Framework.Kafka;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using System.Text;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddDomainContext(options =>
{
    var builder = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile
    ("appsettings.json", optional: true, reloadOnChange: true);

    options.UseSqlServer(builder.Build().GetSection("ConnectionStrings").GetSection("Cart_Db_Conn").Value);

    options.EnableSensitiveDataLogging(false).UseQueryTrackingBehavior(QueryTrackingBehavior.NoTracking);
});

builder.Services.AddControllers();
builder.Services.AddCart();

builder.Services.AddAutoMapper(config =>
{
    config.AddProfile<EntityToDtoProfile>();
});

builder.Services
.AddScoped<ICartService, CartService>()
.AddScoped<ICartRepository, CartRepository>()
.AddScoped<ICartProductService, CartProductService>()
.AddScoped<ICartProductRepository, CartProductRepository>()
.AddScoped<IProductRepository, ProductRepository>()
.AddScoped<IProductService, ProductService>()
.AddScoped<Query>()
.AddScoped<CartQuery>()
.AddScoped<CartProductQuery>()
.AddScoped<Mutation>()
.AddScoped<CartMutation>()
.AddScoped<CartProductMutation>()
.AddGraphQLServer()
.AddQueryType<Query>()
.AddTypeExtension<CartQuery>()
.AddTypeExtension<CartProductQuery>()
.AddMutationType<Mutation>()
.AddTypeExtension<CartMutation>()
.AddTypeExtension<CartProductMutation>()
.AddAuthorization();

builder.Services.AddAuthentication(opt =>
{
    opt.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
    opt.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
})
.AddJwtBearer("Bearer", opt => {
    var Configuration = builder.Configuration;
    opt.TokenValidationParameters = new TokenValidationParameters
    {
        ValidateIssuer = true,
        ValidateAudience = true,
        ValidateLifetime = true,
        ValidateIssuerSigningKey = true,
        ValidIssuer = Configuration["JWT:ValidIssuer"],
        ValidAudience = Configuration["JWT:ValidAudience"],
        IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Configuration["JWT:Secret"]))
    };

    opt.Events = new JwtBearerEvents
    {
        OnChallenge = context =>
        {
            context.Response.OnStarting(async () =>
            {
                await context.Response.WriteAsync("Account not authorized");
            });
            return Task.CompletedTask;
        },
        OnForbidden = context =>
        {
            context.Response.OnStarting(async () =>
            {
                await context.Response.WriteAsync("Account forbidden");
            });
            return Task.CompletedTask;
        }
    };

});

builder.Services.AddEventBus();
builder.Services.AddKafkaProducerAndConsumer();

// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.UseAuthentication();

app.MapControllers();

app.MapGraphQL();

app.Run();

﻿namespace Cart.GraphQL.Schema.Mutations
{
    public class CartProductTypeInput
    {
        public Guid CartId { get; set; }
        public Guid ProductId { get; set; }
        public int Quantity { get; set; } 
    }
}

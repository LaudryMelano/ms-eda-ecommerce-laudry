﻿using Cart.Domain.Dtos;
using Cart.Domain.Services;
using HotChocolate.Authorization;
using System.Data;

namespace Cart.GraphQL.Schema.Mutations
{
    [ExtendObjectType(Name = "Mutation")]
    public class CartProductMutation
    {
        private readonly ICartProductService _service;

        public CartProductMutation(ICartProductService service)
        {
            _service = service;
        }

        [Authorize(Roles = new[] { "admin", "customer" })]
        public async Task<CartProductDto> addCartProduct(CartProductTypeInput input)
        {
            CartProductDto dto = new CartProductDto();
            dto.CartId = input.CartId;
            dto.ProductId = input.ProductId;
            dto.Quantity = input.Quantity;

            var result = await _service.AddCartProduct(dto);

            return result;
        }

        //[Authorize(Roles = new[] { "admin", "customer" })]
        //public async Task<CartProductDto> updateCartProduct(CartProductTypeInput input, Guid id)
        //{
        //    CartProductDto dto = new CartProductDto();
        //    dto.Id = id;
        //    dto.CartId = input.CartId;
        //    dto.ProductId = input.ProductId;
        //    dto.Quantity = input.Quantity;
        //    var result = await _service.UpdateCartProduct(dto, id);

        //    if (!result)
        //    {
        //        throw new GraphQLException(new Error("CartProduct not Found",
        //           "404"));
        //    }

        //    return dto;
        //}

    }
}

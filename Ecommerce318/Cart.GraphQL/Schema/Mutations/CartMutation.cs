﻿using Cart.Domain;
using Cart.Domain.Dtos;
using Cart.Domain.Services;
using HotChocolate.Authorization;
using System.Data;

namespace Cart.GraphQL.Schema.Mutations
{
    [ExtendObjectType(Name = "Mutation")]
    public class CartMutation
    {
        private readonly ICartService _service;

        public CartMutation(ICartService service)
        {
            _service = service;
        }

        [Authorize(Roles = new[] { "admin", "customer" })]
        public async Task<CartDto> addCart(CartTypeInput input)
        {
            CartDto dto = new CartDto();
            dto.CustomerId = input.CustomerId;
            var result = await _service.AddCart(dto);

            return result;
        }

        [Authorize(Roles = new[] { "admin", "customer" })]
        public async Task<CartDto> confirmCartStatus(Guid id)
        {
            var result = await _service.UpdateCartStatus(CartStatusEnum.Confirmed, id);
            if (!result)
            {
                throw new GraphQLException(new Error("Cart not found", "404"));
            }

            return await _service.GetCartById(id);
        }
    }
}

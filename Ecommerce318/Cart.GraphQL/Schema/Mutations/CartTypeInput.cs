﻿using Cart.Domain;

namespace Cart.GraphQL.Schema.Mutations
{
    public class CartTypeInput
    {
        public Guid CustomerId { get; set; }
    }
}

﻿using Cart.Domain.Dtos;
using Cart.Domain.Entities;
using Cart.Domain.Services;
using HotChocolate.Authorization;
using System.Data;

namespace Cart.GraphQL.Schema.Querires
{
    [ExtendObjectType(Name = "Query")]
    public class CartQuery
    {
        private readonly ICartService _service;

        public CartQuery(ICartService service)
        {
            _service = service;
        }

        [Authorize(Roles = new[] { "admin", "customer" })]
        public async Task<IEnumerable<CartDto>> getAllCart()
        {
            IEnumerable<CartDto> result = await _service.All();
            return result;
        }

        [Authorize(Roles = new[] { "admin", "customer" })]
        public async Task<IEnumerable<CartEntity>> getAllInCart()
        {
           return await _service.AllIn();
        }


        [Authorize(Roles = new[] { "admin", "customer" })]
        public async Task<CartDto> getCartById(Guid id)
        {
            CartDto result = await _service.GetCartById(id);
            return result;
        }
    }
}

﻿using Cart.Domain.Dtos;
using Cart.Domain.Services;
using HotChocolate.Authorization;
using Microsoft.EntityFrameworkCore.Query;
using System.Data;

namespace Cart.GraphQL.Schema.Querires
{
    [ExtendObjectType(Name = "Query")]
    public class CartProductQuery
    {
        private readonly ICartProductService _service;

        public CartProductQuery(ICartProductService service)
        {
            _service = service;
        }

        [Authorize(Roles = new[] { "admin", "customer" })]
        public async Task<IEnumerable<CartProductDto>> getAllCartProduct()
        {
            IEnumerable<CartProductDto> result = await _service.All();
            return result;
        }

        [Authorize(Roles = new[] { "admin", "customer" })]
        public async Task<CartProductDto> getCartProductById(Guid id)
        {
            CartProductDto result = await _service.GetCartProductById(id);
            return result;
        }

        [Authorize(Roles = new[] { "admin", "customer" })]
        public async Task<IEnumerable<CartProductDto>> getCartProductByIdCart(Guid id)
        {
            IEnumerable<CartProductDto> result = await _service.GetCartProductByIdCart(id);
            return result;
        }

    }
}

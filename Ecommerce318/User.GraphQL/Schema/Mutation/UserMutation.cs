﻿using User.Domain;
using User.Domain.Dtos;
using User.Domain.Services;

namespace User.GraphQL.Schema.Mutation
{
    [ExtendObjectType(Name = "Mutation")]
    public class UserMutation
    {
        private readonly IUserService _service;

        public UserMutation(IUserService service)
        {
            _service = service;
        }

        public async Task<UserDto> addUser(UserTypeInput input)
        {
            UserDto dto = new UserDto();
            dto.UserName = input.UserName;
            dto.Password = input.Password;
            dto.Email = input.Email;
            dto.FirstName = input.FirstName;
            dto.LastName = input.LastName;
            var result = await _service.AddUser(dto);

            return result;
        }

        public async Task<UserDtoUpdate> updateUser(UserTypeInput input, Guid id)
        {
            UserDtoUpdate dto = new UserDtoUpdate();
            dto.Id = id;
            dto.UserName = input.UserName;
            dto.Password = input.Password;
            dto.Email = input.Email;
            dto.FirstName = input.FirstName;
            dto.LastName = input.LastName;
            var result = await _service.UpdateUser(dto, id);
            if (!result)
            {
                throw new GraphQLException(new Error("User not Found",
                    "404"));
            }
            return dto;
        }

        public async Task<UserDto> updateUserStatus(UserStatusEnum status, Guid id)
        {
            var result = await _service.UpdateUserStatus(status, id);
            if (!result)
            {
                throw new GraphQLException(new Error("User not found", "404"));
            }
            UserDto dto = await _service.GetUserById(id);
            return dto;
        }

        public async Task<UserDto> updateUserType(UserTypeEnum type, Guid id)
        {
            var result = await _service.UpdateUserType(type, id);
            if (!result)
            {
                throw new GraphQLException(new Error("User not found", "404"));
            }
            UserDto dto = await _service.GetUserById(id);
            return dto;
        }
    }
}

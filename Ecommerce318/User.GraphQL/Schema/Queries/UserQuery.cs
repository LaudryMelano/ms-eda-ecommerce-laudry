﻿using User.Domain.Dtos;
using User.Domain.Services;

namespace User.GraphQL.Schema.Queries
{
    [ExtendObjectType(Name = "Query")]
    public class UserQuery
    {
        private readonly IUserService _service;

        public UserQuery(IUserService service)
        {
            _service = service;
        }

        public async Task<IEnumerable<UserDto>> getAllUser()
        {
            IEnumerable<UserDto> result = await _service.All();
            return result;
        }

        public async Task<UserDto> getUserById(Guid id)
        {
            UserDto result = await _service.GetUserById(id);
            return result;
        }
    }
}

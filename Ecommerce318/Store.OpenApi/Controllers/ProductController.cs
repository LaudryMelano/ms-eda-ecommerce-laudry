﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Store.Domain.Dtos;
using Store.Domain.Services;
using Store.Domain;
using FluentValidation;
using FluentValidation.Results;

namespace Store.OpenApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductController : ControllerBase
    {
        private readonly IProductService _service;
        private readonly ILogger<ProductController> _logger;
        private IValidator<ProductDto> _validator;
        private IValidator<ProductDtoUpdate> _validatorUpdate;

        public ProductController(IProductService service, IValidator<ProductDto> validator, IValidator<ProductDtoUpdate> validatorUpdate, ILogger<ProductController> logger)
        {
            _service = service;
            _logger = logger;
            _validator = validator;
            _validatorUpdate = validatorUpdate;
        }

        [HttpGet("GetAllProduct")]
        public async Task<IActionResult> Get()
        {

            return Ok(await _service.All());
        }

        [HttpPost("PostProduct")]
        public async Task<IActionResult> Post([FromBody] ProductDto payload, CancellationToken
            cancellationToken)
        {
            try
            {
                ValidationResult result = _validator.Validate(payload);
                if (!result.IsValid)
                    return BadRequest(result);

                var dto = await _service.AddProduct(payload);
                if (dto != null)
                {
                    return Ok(dto);
                }
            }
            catch (OperationCanceledException ex) when (cancellationToken.IsCancellationRequested)
            {
                _logger.LogWarning(ex.Message);
                throw;
            }
            return BadRequest();
        }

        [HttpPut("EditProduct")]
        public async Task<IActionResult> Put([FromBody] ProductDtoUpdate payload, Guid id, CancellationToken cancellationToken)
        {
            try
            {
                if (payload != null)
                {
                    ValidationResult result = _validatorUpdate.Validate(payload);
                    if (!result.IsValid)
                        return BadRequest(result);

                    payload.Id = id;
                    var isUpdate = await _service.UpdateProduct(payload, id);
                    if (isUpdate)
                        return Ok(isUpdate);
                }
            }
            catch (OperationCanceledException ex) when (cancellationToken.IsCancellationRequested)
            {
                _logger.LogWarning(ex.Message);
                throw;
            }
            return NoContent();
        }

        [HttpPut("EditCategoryStatus")]
        public async Task<IActionResult> Put(Guid id, StoreStatusEnum status, CancellationToken cancellationToken)
        {
            try
            {
                if (status != null)
                {
                    var isUpdate = await _service.UpdateProductStatus(status, id);
                    if (isUpdate)
                        return Ok(isUpdate);
                }
            }
            catch (OperationCanceledException ex) when (cancellationToken.IsCancellationRequested)
            {
                _logger.LogWarning(ex.Message);
                throw;
            }
            return NoContent();
        }

        [HttpDelete("DeleteCategory")]
        public async Task<IActionResult> Delete(Guid id, CancellationToken cancellationToken)
        {
            try
            {
                if (id != null)
                {
                    var isDelete = await _service.DeleteProduct(id);
                    if (isDelete)
                        return Ok(isDelete);
                }
            }
            catch (OperationCanceledException ex) when (cancellationToken.IsCancellationRequested)
            {
                _logger.LogWarning(ex.Message);
                throw;
            }
            return NoContent();
        }
    }
}

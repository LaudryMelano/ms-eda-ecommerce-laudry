﻿using FluentValidation;
using FluentValidation.Results;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Store.Domain;
using Store.Domain.Dtos;
using Store.Domain.Services;

namespace Store.OpenApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CategoryController : ControllerBase
    {
        private readonly ICategoryService _service;
        private readonly ILogger<CategoryController> _logger;
        private IValidator<CategoryDto> _validator;
        private IValidator<CategoryDtoUpdate> _validatorUpdate;
        public CategoryController(ICategoryService service, IValidator<CategoryDto> validator, IValidator<CategoryDtoUpdate> validatorUpdate, ILogger<CategoryController> logger)
        {
            _service = service;
            _logger = logger;
            _validator = validator;
            _validatorUpdate = validatorUpdate;
        }

        [HttpGet("GetAllCategory")]
        public async Task<IActionResult> Get()
        {

            return Ok(await _service.All());
        }

        [HttpPost("PostCategory")]
        public async Task<IActionResult> Post([FromBody] CategoryDto payload,  CancellationToken
            cancellationToken)
        {
            try
            {
                ValidationResult result = await _validator.ValidateAsync(payload);
                if (!result.IsValid)
                    return BadRequest(result);
                var dto = await _service.AddCategory(payload);
                if (dto != null)
                {
                    return Ok(dto);
                }
            }
            catch (OperationCanceledException ex) when (cancellationToken.IsCancellationRequested)
            {
                _logger.LogWarning(ex.Message);
                throw;
            }
            return BadRequest();
        }

        [HttpPut("EditCategory")]
        public async Task<IActionResult> Put([FromBody] CategoryDtoUpdate payload, Guid id, CancellationToken cancellationToken)
        {
            try
            {
                if (payload != null)
                {
                    ValidationResult result = await _validatorUpdate.ValidateAsync(payload);
                    if (!result.IsValid)
                        return BadRequest(result);

                    payload.Id= id;
                    var isUpdate = await _service.UpdateCategory(payload, id);
                    if (isUpdate)
                        return Ok(isUpdate);
                }
            }
            catch (OperationCanceledException ex) when (cancellationToken.IsCancellationRequested)
            {
                _logger.LogWarning(ex.Message);
                throw;
            }
            return NoContent();
        }

        [HttpPut("EditCategoryStatus")]
        public async Task<IActionResult> Put(Guid id, StoreStatusEnum status, CancellationToken cancellationToken)
        {
            try
            {
                if (status != null)
                {
                    var isUpdate = await _service.UpdateCategoryStatus(status, id);
                    if (isUpdate)
                        return Ok(isUpdate);
                }
            }
            catch (OperationCanceledException ex) when (cancellationToken.IsCancellationRequested)
            {
                _logger.LogWarning(ex.Message);
                throw;
            }
            return NoContent();
        }

        [HttpDelete("DeleteCategory")]
        public async Task<IActionResult> Delete(Guid id, CancellationToken cancellationToken)
        {
            try
            {
                if (id != null)
                {
                    var isDelete = await _service.DeleteCategory(id);
                    if (isDelete)
                        return Ok(isDelete);
                }
            }
            catch (OperationCanceledException ex) when (cancellationToken.IsCancellationRequested)
            {
                _logger.LogWarning(ex.Message);
                throw;
            }
            return NoContent();
        }

    }
}

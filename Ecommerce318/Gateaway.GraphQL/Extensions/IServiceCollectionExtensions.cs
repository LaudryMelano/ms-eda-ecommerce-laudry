﻿using HotChocolate.Authorization;
using System.Data;

namespace Gateaway.GraphQL.Extensions
{
   
    public static class IServiceCollectionExtensions
    {
        public const string LookUps = "LookUpServies";
        public const string Stores = "StoreService";
        public const string Carts = "CartService";
        public const string Users = "UserService";
        public const string Payments = "PaymentService";

        [Authorize]
        public static IServiceCollection AddHttpClientServices(this IServiceCollection services)
        {

            services.AddHttpClient(LookUps, c => c.BaseAddress = new Uri("https://localhost:7205/graphql"));
            services.AddHttpClient(Stores, c => c.BaseAddress = new Uri("https://localhost:7277/graphql"));
            services.AddHttpClient(Carts, c => c.BaseAddress = new Uri("https://localhost:7187/graphql"));
            services.AddHttpClient(Users, c => c.BaseAddress = new Uri("https://localhost:7210/graphql"));
            services.AddHttpClient(Payments, c => c.BaseAddress = new Uri("https://localhost:7075/graphql"));


            services
                .AddGraphQLServer()
                .AddRemoteSchema(LookUps)
                .AddRemoteSchema(Stores)
                .AddRemoteSchema(Carts)
                .AddRemoteSchema(Users)
                .AddRemoteSchema(Payments); 

            return services;
        }
    }
}

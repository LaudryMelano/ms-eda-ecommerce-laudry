﻿using Microsoft.EntityFrameworkCore;
using Store.Domain.Entities.Configurations;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Store.Domain.Entities
{
    public class StoreDbContext : DbContext
    {
        public StoreDbContext(DbContextOptions<StoreDbContext> options) : base(options)
        {

        }

        public DbSet<CategoryEntity> Categories { get; set; }
        public DbSet<ProductsEntity> Products { get; set; }
        public DbSet<AtrributeEntity> Attributes { get; set; }

        public DbSet<GalleryEntity> Galleries { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //base.OnModelCreating(modelBuilder);
            modelBuilder.ApplyConfiguration(new CategoryConfiguration());
            modelBuilder.ApplyConfiguration(new ProductsConfiguration());
            modelBuilder.ApplyConfiguration(new AttributeConfiguration());
            modelBuilder.ApplyConfiguration(new GalleryConfiguration());
        }

        public static DbContextOptions<StoreDbContext> OnConfigure()
        {
            var optionsBuilder = new DbContextOptionsBuilder<StoreDbContext>();
            var builder = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);
            optionsBuilder.UseSqlServer(builder.Build()
                .GetSection("ConnectionStrings").GetSection("Store_Db_Conn").Value);

            return optionsBuilder.Options;
        }
    }
}

﻿
using Store.Domain;

namespace Store.Domain.Entities
{
    public class AtrributeEntity
    {
        public Guid Id { get; set; }
        public AttributeTypeEnum Type { get; set; } = default!;
        public string Unit { get; set; } = default!;
        public LookUpStatusEnum Status { get; set; } = default!;
        public DateTime Modified { get; internal set; } = DateTime.Now;
    }
}

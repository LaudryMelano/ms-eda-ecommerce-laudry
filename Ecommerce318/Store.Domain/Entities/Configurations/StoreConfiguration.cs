﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Store.Domain.Entities.Configurations
{
    public class CategoryConfiguration : IEntityTypeConfiguration<CategoryEntity>
    {
        public void Configure(EntityTypeBuilder<CategoryEntity> builder)
        {
            builder.ToTable("Categories");
            builder.HasKey(e => e.Id);
            builder.Property(e => e.Id).IsRequired();
            builder.Property(e => e.Name).IsRequired();
            builder.Property(e => e.Description).IsRequired();
            builder.Property(e => e.Status).IsRequired();
        }
    }

    public class ProductsConfiguration : IEntityTypeConfiguration<ProductsEntity>
    {
        public void Configure(EntityTypeBuilder<ProductsEntity> builder)
        {
            builder.ToTable("Products");
            builder.HasKey(e => e.Id);
            builder.Property(e => e.Id).IsRequired();
            builder.Property(e => e.CategoryId).IsRequired();
            builder.Property(e => e.AttributeId).IsRequired();
            builder.Property(e => e.GalleryId).IsRequired(false).HasDefaultValue(null);
            builder.Property(e => e.SKU).HasMaxLength(20).IsRequired();
            builder.Property(e => e.Name).HasMaxLength(30).IsRequired();
            builder.Property(e => e.Description).IsRequired();
            builder.Property(e => e.Price).HasPrecision(18,2);
            builder.Property(e => e.Volume).HasPrecision(18,2);
            builder.Property(e => e.Status).IsRequired();
        }
    }

    public class AttributeConfiguration : IEntityTypeConfiguration<AtrributeEntity>
    {
        public void Configure(EntityTypeBuilder<AtrributeEntity> builder)
        {
            builder.ToTable("Attributes");
            builder.HasKey(e => e.Id);
            builder.Property(e => e.Id).IsRequired();
            builder.Property(e => e.Type).IsRequired();
            builder.Property(e => e.Unit).HasMaxLength(30).IsRequired();
            builder.Property(e => e.Status).IsRequired();
        }
    }

    public class GalleryConfiguration : IEntityTypeConfiguration<GalleryEntity>
    {
        public void Configure(EntityTypeBuilder<GalleryEntity> builder)
        {
            builder.ToTable("Galeries");
            builder.HasKey(e => e.Id);
            builder.Property(e => e.Id).IsRequired();
            builder.Property(e => e.FileLink).HasMaxLength(255).IsRequired();
            builder.Property(e => e.Name).HasMaxLength(30).IsRequired();
            builder.Property(e => e.Description).IsRequired();
            builder.Property(e => e.Status).IsRequired();
        }
    }


}

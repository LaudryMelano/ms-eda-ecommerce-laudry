﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Store.Domain.Entities
{
    public class ProductsEntity
    {
        public Guid Id { get; set; }
        public Guid CategoryId { get; set; }
        public Guid AttributeId { get; set; }
        public Guid? GalleryId { get; set; }
        public string SKU { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public decimal Price { get; set; }
        public decimal Volume { get; set; }
        public int Sold { get; set; }
        public int Stock { get; set; }
        public StoreStatusEnum Status { get; set; } = default!;
        public DateTime Modified { get; internal set; } = DateTime.Now;

        //Relationship
        [ForeignKey("CategoryId")]
        public virtual CategoryEntity Category { get; set; }

        [ForeignKey("AttributeId")]
        public virtual AtrributeEntity Attribute { get; set; }

        [ForeignKey("GalleryId")]
        public virtual GalleryEntity Galery { get; set; }

    }
}

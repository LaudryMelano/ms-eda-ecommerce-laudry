﻿using FluentValidation;
using Store.Domain.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Store.Domain.Validations
{
    public class ProductValidator: AbstractValidator<ProductDto>
    {
        public ProductValidator()
        {
            int i = 0;
            RuleFor(x => x.Name).NotEmpty().WithMessage("Name cannot empty");
            RuleFor(x => x.Description).MinimumLength(5).WithMessage("Description must consist at least 5 character!");
            RuleFor(x => x.SKU).Length(5, 20).WithMessage("SKU must consist at least 5 character!")
                .Must(x => int.TryParse(x, out i)).WithMessage("SKU must consist of numbers!");
            RuleFor(x => x.Price).NotEmpty().WithMessage("Price cannot empty");
            RuleFor(x => x.Volume).Must(x=>x>0).WithMessage("Volume cannot empty");
            RuleFor(x => x.Stock).NotEmpty().WithMessage("Stock cannot empty");

        }
    }

    public class ProductValidatorUpdate : AbstractValidator<ProductDtoUpdate>
    {
        public ProductValidatorUpdate()
        {
            int i = 0;
            RuleFor(x => x.Name).NotEmpty().WithMessage("Name cannot empty");
            RuleFor(x => x.Description).MinimumLength(5).WithMessage("Description must consist at least 5 character!");
            RuleFor(x => x.SKU).Length(5, 20).WithMessage("SKU must consist at least 5 character!")
                               .Must(x => int.TryParse(x, out i)).WithMessage("SKU must consist of numbers!");
        }
    }
}

﻿using FluentValidation;
using Store.Domain.Dtos;

namespace Store.Domain.Validations
{
    public class CategoryValidator: AbstractValidator<CategoryDto>
    {
        public CategoryValidator()
        {
            RuleFor(x=> x.Name).NotEmpty().WithMessage("Name cannot empty");
            RuleFor(x=> x.Description).MinimumLength(5).WithMessage("Description must consist at least 5 character!");
        } 
    }

    public class CategoryValidatorUpdate : AbstractValidator<CategoryDtoUpdate>
    {
        public CategoryValidatorUpdate()
        {
            RuleFor(x => x.Name).NotEmpty().WithMessage("Name cannot empty");
            RuleFor(x => x.Description).MinimumLength(5).WithMessage("Description must consist at least 5 character!");
        }
    }
}

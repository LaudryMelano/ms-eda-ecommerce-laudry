﻿using Microsoft.EntityFrameworkCore;
using Store.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Store.Domain.Repositories
{
    public interface IProductRepository
    {
        Task<int> GetCount();
        Task<IEnumerable<ProductsEntity>> GetAll();
        Task<IEnumerable<ProductsEntity>> GetPaged(int page, int size);
        Task<ProductsEntity> GetById(Guid id);
        Task<ProductsEntity> Add(ProductsEntity entity);
        Task<ProductsEntity> Update(ProductsEntity entity);
        void Delete(ProductsEntity entity);
        Task<int> SaveChangesAsync(CancellationToken cancellationToken = default);

    }
    public class ProductRepository : IProductRepository
    {
        protected readonly StoreDbContext _context;

        public ProductRepository(StoreDbContext context)
        {
            _context = context;
            _context.Database.EnsureCreated();
        }
        public async Task<ProductsEntity> Add(ProductsEntity entity)
        {
            _context.Set<ProductsEntity>().Add(entity);
            return entity;
        }

        public void Delete(ProductsEntity entity)
        {
            _context.Set<ProductsEntity>().Update(entity);
        }

        public async Task<IEnumerable<ProductsEntity>> GetAll()
        {
            return await _context.Set<ProductsEntity>().Where(x => !x.Status.Equals(StoreStatusEnum.Removed)).ToListAsync();
        }

        public async Task<ProductsEntity> GetById(Guid id)
        {
            return await _context.Set<ProductsEntity>().FindAsync(id);
        }

        public Task<int> GetCount()
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<ProductsEntity>> GetPaged(int page, int size)
        {
            throw new NotImplementedException();
        }

        public async Task<int> SaveChangesAsync(CancellationToken cancellationToken = default)
        {
            return await _context.SaveChangesAsync(cancellationToken);
        }

        public async Task<ProductsEntity> Update(ProductsEntity entity)
        {
            _context.Set<ProductsEntity>().Update(entity);
            return entity;
        }

        public virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                _context.Dispose();
            }
        }

        public void Dispose()
        {
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }
    }
}

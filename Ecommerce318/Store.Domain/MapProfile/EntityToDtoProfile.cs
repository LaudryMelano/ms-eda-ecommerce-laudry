﻿using Store.Domain.Dtos;
using Store.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using System.Linq.Expressions;

namespace Store.Domain.MapProfile
{
    public class EntityToDtoProfile: Profile
    {
        

        public EntityToDtoProfile() : base("Entity to Dtos profile")
        {
            CreateMap<CategoryEntity, CategoryDto>();
            CreateMap<CategoryDto, CategoryEntity>();
            CreateMap<CategoryEntity, CategoryDtoUpdate>();
            CreateMap<CategoryDtoUpdate, CategoryEntity>();

            CreateMap<ProductsEntity, ProductDto>();
            CreateMap<ProductDto, ProductsEntity>();
            CreateMap<ProductsEntity, ProductDtoUpdate>();
            CreateMap<ProductDtoUpdate, ProductsEntity>();

            CreateMap<GalleryEntity, GalleryDto>();
            CreateMap<GalleryDto, GalleryEntity>();
        }
    }
}

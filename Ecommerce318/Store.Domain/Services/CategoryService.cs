﻿using AutoMapper;
using Store.Domain.Dtos;
using Store.Domain.Entities;
using Store.Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Store.Domain.Services
{
    public interface ICategoryService
    {
        Task<IEnumerable<CategoryDto>> All();
        Task<CategoryDto> AddCategory(CategoryDto dto);
        Task<CategoryDto> GetCategoryById(Guid id);
        Task<bool> UpdateCategory(CategoryDtoUpdate dto, Guid id);
        Task<bool> UpdateCategoryStatus(StoreStatusEnum status, Guid id);
        Task<bool> DeleteCategory(Guid id);
    }
    public class CategoryService : ICategoryService
    {
        private ICategoryRepository _repository;
        public readonly IMapper _mapper;

        public CategoryService(ICategoryRepository repository, IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }
        public async Task<CategoryDto> AddCategory(CategoryDto dto)
        {
            if (dto != null)
            {
                dto.Status = StoreStatusEnum.Inactive;
                var dtoToEntity = _mapper.Map<CategoryEntity>(dto);
                var entity = await _repository.Add(dtoToEntity);
                var result = await _repository.SaveChangesAsync();

                if (result > 0)
                {
                    return _mapper.Map<CategoryDto>(entity);
                }
            }
            return new CategoryDto();
        }

        public async Task<IEnumerable<CategoryDto>> All()
        {
            return _mapper.Map<IEnumerable<CategoryDto>>(await _repository.GetAll());
        }

        public async Task<CategoryDto> GetCategoryById(Guid id)
        {
            if (id != Guid.Empty)
            {
                var result = await _repository.GetById(id);
                if (result != null)
                {
                    return _mapper.Map<CategoryDto>(result);
                }
            }
            return null;
        }

        public async Task<bool> UpdateCategory(CategoryDtoUpdate dto, Guid id)
        {
            if (dto != null)
            {
                var category = await _repository.GetById(id);
                if (category != null)
                {
                    var entity = await _repository.Update(_mapper.Map<CategoryEntity>(dto));
                    entity.Status = category.Status;
                    var result = await _repository.SaveChangesAsync();

                    if (result > 0)
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        public async Task<bool> UpdateCategoryStatus(StoreStatusEnum status, Guid id)
        {
            if (id != null)
            {
                var category = await _repository.GetById(id);
                if (category != null)
                {
                    category.Status = status;
                    var entity = await _repository.Update(category);
                    var result = await _repository.SaveChangesAsync();

                    if (result > 0)
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        public async Task<bool> DeleteCategory(Guid id)
        {
            if (id != null)
            {
                var category = await _repository.GetById(id);
                if (category != null)
                {
                    category.Status = StoreStatusEnum.Removed;
                    _repository.Delete(category);
                    var result = await _repository.SaveChangesAsync();

                    if (result > 0)
                    {
                        return true;
                    }
                }
            }
            return false;
        }
    }
}

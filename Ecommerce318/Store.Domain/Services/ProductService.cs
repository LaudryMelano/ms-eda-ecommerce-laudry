﻿using AutoMapper;
using Framework.Core.Events;
using Framework.Core.Events.Externals;
using Store.Domain.Dtos;
using Store.Domain.Entities;
using Store.Domain.EventEnvelopes.Product;
using Store.Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Store.Domain.Services
{
    public interface IProductService
    {
        Task<IEnumerable<ProductDto>> All();
        Task<ProductDto> AddProduct(ProductDto dto);
        Task<ProductDto> GetProductById(Guid id);
        Task<bool> UpdateProduct(ProductDtoUpdate dto, Guid id);
        Task<bool> UpdateProductStatus(StoreStatusEnum status, Guid id);
        Task<bool> DeleteProduct(Guid id);

    }
    public class ProductService : IProductService
    {
        private IProductRepository _repository;
        public readonly IMapper _mapper;
        private readonly IExternalEventProducer _externalEventProducer;

        public ProductService(IProductRepository repository, IMapper mapper,
            IExternalEventProducer externalEventProducer)
        {
            _repository = repository;
            _mapper = mapper;
            _externalEventProducer = externalEventProducer;
        }
        public async Task<ProductDto> AddProduct(ProductDto dto)
        {
            if (dto != null)
            {
                
                dto.Status = StoreStatusEnum.Inactive;
                var dtoToEntity = _mapper.Map<ProductsEntity>(dto);
                var entity = await _repository.Add(dtoToEntity);
                var result = await _repository.SaveChangesAsync();

                if (result > 0)
                {
                    var externalEvent = new EventEnvelope<ProductCreated>(
                    ProductCreated.Create(
                        entity.Id,
                        entity.CategoryId,
                        entity.AttributeId,
                        entity.SKU,
                        entity.Name,
                        entity.Description,
                        entity.Price,
                        entity.Volume,
                        entity.Sold,
                        entity.Stock,
                        entity.Status,
                        entity.Modified
                        )
                    );

                    await _externalEventProducer.Publish(externalEvent, new CancellationToken());
                    return _mapper.Map<ProductDto>(entity);
                }
            }
            return new ProductDto();
        }

        public async Task<IEnumerable<ProductDto>> All()
        {
            return _mapper.Map<IEnumerable<ProductDto>>(await _repository.GetAll());
        }

        public async Task<bool> DeleteProduct(Guid id)
        {
            if (id != null)
            {
                var category = await _repository.GetById(id);
                if (category != null)
                {
                    category.Status = StoreStatusEnum.Removed;
                    _repository.Delete(category);
                    var result = await _repository.SaveChangesAsync();

                    if (result > 0)
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        public async Task<ProductDto> GetProductById(Guid id)
        {
            if (id != Guid.Empty)
            {
                var result = await _repository.GetById(id);
                if (result != null)
                {
                    return _mapper.Map<ProductDto>(result);
                }
            }
            return null;
        }

        public async Task<bool> UpdateProduct(ProductDtoUpdate dto, Guid id)
        {
            if (dto != null)
            {
                var products = await _repository.GetById(id);
                if (products != null)
                {
                    var entity = await _repository.Update(_mapper.Map<ProductsEntity>(dto));
                    entity.Status = products.Status;
                    var result = await _repository.SaveChangesAsync();

                    if (result > 0)
                    {
                        var externalEvent = new EventEnvelope<ProductUpdated>(
                        ProductUpdated.Update(
                        entity.Id,
                        entity.CategoryId,
                        entity.AttributeId,
                        entity.SKU,
                        entity.Name,
                        entity.Description,
                        entity.Price,
                        entity.Volume,
                        entity.Sold,
                        entity.Stock,
                        entity.Modified
                        )
                    );

                        await _externalEventProducer.Publish(externalEvent, new CancellationToken());
                        return true;
                    }
                }
            }
            return false;
        }

        public async Task<bool> UpdateProductStatus(StoreStatusEnum status, Guid id)
        {
            if (id != null)
            {
                var product = await _repository.GetById(id);
                if (product != null)
                {
                    product.Status = status;
                    product.Modified = DateTime.Now;
                    var entity = await _repository.Update(product);
                    var result = await _repository.SaveChangesAsync();

                    if (result > 0)
                    {
                        var externalEvent = new EventEnvelope<ProductStatusChanged>(
                            ProductStatusChanged.UpdateStatus(
                                entity.Id,
                                entity.Status,
                                entity.Modified
                                )
                            );

                        await _externalEventProducer.Publish(externalEvent, new CancellationToken());
                        return true;
                    }
                }
            }
            return false;
        }
    }
}

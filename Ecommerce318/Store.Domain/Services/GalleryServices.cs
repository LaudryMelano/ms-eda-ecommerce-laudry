﻿using AutoMapper;
using Store.Domain.Dtos;
using Store.Domain.Entities;
using Store.Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Store.Domain.Services
{
    public interface IGalleryServices
    {
        Task<IEnumerable<GalleryDto>> All();

        Task<GalleryDto> GetById(Guid id);

        Task<GalleryDto> Add(GalleryDto dto);
    }
    public class GalleryServices : IGalleryServices
    {
        private IGalleryRepository _repository;
        private readonly IMapper _mapper;

        public GalleryServices(IGalleryRepository repository, IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }

        public async Task<IEnumerable<GalleryDto>> All()
        {
           return _mapper.Map<IEnumerable<GalleryDto>>(await _repository.GetAll());
        }

        public Task<GalleryDto> GetById(Guid id)
        {
            throw new NotImplementedException();
        }

        public async Task<GalleryDto> Add(GalleryDto dto)
        {
            if (dto != null)
            {
                dto.Status = StoreStatusEnum.Active;
                var entity = _mapper.Map<GalleryEntity>(dto);

                await _repository.Add(entity);

                var result = await _repository.SaveChangesAsync();

                if (result > 0)
                {
                    return _mapper.Map<GalleryDto>(entity);
                }

            }
            return dto;
        }
    }
}

﻿using FluentValidation;
using LookUpDomain.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace LookUpDomain.Validations
{
    public class CurrencyValidator: AbstractValidator<CurrencyDto>
    {
        public CurrencyValidator() 
        {
            RuleFor(x => x.Name).NotEmpty().WithMessage("Name cannot empty!");
            RuleFor(x => x.Code).Length(3, 3).Matches(@"[A-Z]");
            RuleFor(x => x.Symbol).Length(1,3).Matches(@"^\D*$");
        }  
    }
}

﻿using FluentValidation;
using LookUpDomain.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LookUpDomain.Validations
{
    public class AttributeValidator: AbstractValidator<AttributeDto>
    {
        public AttributeValidator()
        {
            RuleFor(x => x.Type).NotNull().WithMessage("Type cannot null!").IsInEnum();
            RuleFor(x => x.Unit).Length(5,30).WithMessage("Unit must consist at least 5 character!");
        }
    }

    public class AttributeValidatorUpdate : AbstractValidator<AttributeDtoUpdate>
    {
        public AttributeValidatorUpdate()
        {
            RuleFor(x => x.Type).NotNull().WithMessage("Type cannot null!").IsInEnum();
            RuleFor(x => x.Unit).Length(5, 30).WithMessage("Unit must consist at least 5 character!");
        }
    }
}

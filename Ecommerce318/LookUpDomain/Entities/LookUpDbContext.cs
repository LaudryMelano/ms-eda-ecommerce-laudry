﻿
using LookUpDomain.Entities.Configurations;
using Microsoft.EntityFrameworkCore;

namespace LookUpDomain.Entities
{
    public class LookUpDbContext: DbContext
    {
        public LookUpDbContext(DbContextOptions<LookUpDbContext> options): base(options) 
        {

        }

        public DbSet<AtrributeEntity> Atrributes { get; set; }
        public DbSet<CurrencyEntity> Currencies { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //base.OnModelCreating(modelBuilder);
            modelBuilder.ApplyConfiguration(new AttributeConfiguration());
            modelBuilder.ApplyConfiguration(new CurrencyConfiguration());
        }
    }
}

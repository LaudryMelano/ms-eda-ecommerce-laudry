﻿using System.Text.Json.Serialization;

namespace LookUpDomain
{
    [JsonConverter(typeof(JsonStringEnumConverter))]
    public enum AttributeTypeEnum
    {
        Text,
        Number,
        Decimal
    }
}

﻿using LookUpDomain.Entities;
using Microsoft.EntityFrameworkCore;

namespace LookUpDomain.Repositories
{
    public interface IAttributeRepository
    {
        Task<int> GetCount();
        Task<IEnumerable<AtrributeEntity>> GetAll();
        Task<IEnumerable<AtrributeEntity>> GetPaged(int page, int size);
        Task<AtrributeEntity> GetById(Guid id);
        Task<AtrributeEntity> Add(AtrributeEntity entity);
        Task<AtrributeEntity> Update(AtrributeEntity entity);
        void Delete(AtrributeEntity entity);
        Task<int> SaveChangesAsync(CancellationToken cancellationToken = default);

    }
    public class AttributeRepository : IAttributeRepository
    {
        protected readonly LookUpDbContext _context;

        public AttributeRepository(LookUpDbContext context)
        {
            _context = context;
            _context.Database.EnsureCreated();
        }

        public async Task<AtrributeEntity> Add(AtrributeEntity entity)
        {
            _context.Set<AtrributeEntity>().Add(entity);
            return entity;
        }

        public void Delete(AtrributeEntity entity)
        {
            throw new NotImplementedException();
        }

        public async Task<IEnumerable<AtrributeEntity>> GetAll()
        {
            return await _context.Set<AtrributeEntity>().ToListAsync();
        }

        public async Task<AtrributeEntity> GetById(Guid id)
        {
            return await _context.Set<AtrributeEntity>().FindAsync(id);
        }

        public Task<int> GetCount()
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<AtrributeEntity>> GetPaged(int page, int size)
        {
            throw new NotImplementedException();
        }

        public async Task<int> SaveChangesAsync(CancellationToken cancellationToken = default)
        {
            return await _context.SaveChangesAsync(cancellationToken);
        }

        public async Task<AtrributeEntity> Update(AtrributeEntity entity)
        {
            _context.Set<AtrributeEntity>().Update(entity);
            return entity;
        }

        public virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                _context.Dispose();
            }
        }

        public void Dispose()
        {
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }
    }
}

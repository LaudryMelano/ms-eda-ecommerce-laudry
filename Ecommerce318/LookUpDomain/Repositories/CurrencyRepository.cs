﻿using LookUpDomain.Entities;
using Microsoft.EntityFrameworkCore;

namespace LookUpDomain.Repositories
{
    public interface ICurrencyRepository
    {
        Task<int> GetCount();
        Task<IEnumerable<CurrencyEntity>> GetAll();
        Task<IEnumerable<CurrencyEntity>> GetPaged(int page, int size);
        Task<CurrencyEntity> GetById(Guid id);
        Task<CurrencyEntity> Add(CurrencyEntity entity);
        Task<CurrencyEntity> Update(CurrencyEntity entity);
        void Delete(CurrencyEntity entity);
        Task<int> SaveChangesAsync(CancellationToken cancellationToken = default);

    }
    public class CurrencyRepository : ICurrencyRepository
    {
        protected readonly LookUpDbContext _context;

        public CurrencyRepository(LookUpDbContext context)
        {
            _context = context;
            _context.Database.EnsureCreated();
        }

        public async Task<CurrencyEntity> Add(CurrencyEntity entity)
        {
            _context.Set<CurrencyEntity>().Add(entity);
            return entity;
        }

        public void Delete(CurrencyEntity entity)
        {
            throw new NotImplementedException();
        }

        public async Task<IEnumerable<CurrencyEntity>> GetAll()
        {
            return await _context.Set<CurrencyEntity>().ToListAsync();
        }

        public async Task<CurrencyEntity> GetById(Guid id)
        {
            return await _context.Set<CurrencyEntity>().FindAsync(id);
        }

        public Task<int> GetCount()
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<CurrencyEntity>> GetPaged(int page, int size)
        {
            throw new NotImplementedException();
        }

        public async Task<int> SaveChangesAsync(CancellationToken cancellationToken = default)
        {
            return await _context.SaveChangesAsync(cancellationToken);
        }

        public async Task<CurrencyEntity> Update(CurrencyEntity entity)
        {
            _context.Set<CurrencyEntity>().Update(entity);
            return entity;
        }

        public virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                _context.Dispose();
            }
        }

        public void Dispose()
        {
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }
    }
}

﻿using AutoMapper;
using LookUpDomain.Dtos;
using LookUpDomain.Entities;

namespace LookUpDomain.MapProfile
{
    public class EntityToDtoProfile: Profile
    {
        public EntityToDtoProfile(): base("Entity to Dtos profile") 
        {
            CreateMap<AtrributeEntity, AttributeDto>();
            CreateMap<AttributeDto, AtrributeEntity>();
            CreateMap<AtrributeEntity, AttributeDtoUpdate>();
            CreateMap<AttributeDtoUpdate, AtrributeEntity>();
            CreateMap<CurrencyEntity, CurrencyDto>();
            CreateMap<CurrencyDto, CurrencyEntity>();
            CreateMap<CurrencyEntity, CurrencyDtoUpdate>();
            CreateMap<CurrencyDtoUpdate, CurrencyEntity>();
        }
    }
}

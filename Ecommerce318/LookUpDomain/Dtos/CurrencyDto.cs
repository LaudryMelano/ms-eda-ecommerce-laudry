﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LookUpDomain.Dtos
{
    public class CurrencyDto
    {
        public Guid Id { get; set; }
        public string Name { get; set; } 
        public string Code { get; set; } 
        public string Symbol { get; set; } 
        public LookUpStatusEnum Status { get; internal set; } 
    }
}

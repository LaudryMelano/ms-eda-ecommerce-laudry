﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LookUpDomain.Dtos
{
    public class AttributeDtoUpdate
    {
        public Guid Id { get; set; }
        public AttributeTypeEnum Type { get; set; }
        public string Unit { get; set; }
    }
}

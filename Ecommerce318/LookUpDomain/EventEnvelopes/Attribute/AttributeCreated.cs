﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LookUpDomain.EventEnvelopes.Attribute
{
    public record AttributeCreated(
        Guid Id, 
        AttributeTypeEnum Type, 
        string Unit,
        LookUpStatusEnum Status,
        DateTime Modified)
    {
        public static AttributeCreated Create(
            Guid id,
            AttributeTypeEnum type,
            string unit,
            LookUpStatusEnum status,
            DateTime modified
            ) => new(id, type, unit, status, modified);
    }
}

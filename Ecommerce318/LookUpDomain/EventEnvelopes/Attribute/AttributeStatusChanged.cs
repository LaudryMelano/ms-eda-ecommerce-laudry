﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LookUpDomain.EventEnvelopes.Attribute
{
    public record AttributeStatusChanged (
        Guid Id,
        LookUpStatusEnum Status,
        DateTime Modified
        )
    {
        public static AttributeStatusChanged UpdateStatus(
            Guid id,
            LookUpStatusEnum status,
            DateTime modified
            ) => new(id, status, modified);
    }
}

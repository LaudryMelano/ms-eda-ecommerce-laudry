﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LookUpDomain.EventEnvelopes.Attribute
{
    public record AttributeUpdated(
        Guid Id,
        AttributeTypeEnum Type,
        string Unit,
        DateTime Modified
        )
    {
        public static AttributeUpdated Update(
        Guid id,
        AttributeTypeEnum type,
        string unit,
        DateTime modified
            ) => new(id, type, unit, modified);
    }
}

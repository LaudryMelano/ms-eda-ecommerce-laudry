﻿using LookUpDomain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LookUpDomain
{
    public static class ServiceExtention
    {
        public static void AddDomainContext(this IServiceCollection services, 
            Action<DbContextOptionsBuilder> optionAction, 
            ServiceLifetime contextLifeTime = ServiceLifetime.Scoped,
            ServiceLifetime optionLifeTime = ServiceLifetime.Scoped)
        {
            services.AddDbContext<LookUpDbContext>(optionAction, contextLifeTime, optionLifeTime);

        }
    }
}

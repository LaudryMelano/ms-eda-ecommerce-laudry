﻿using AutoMapper;
using Framework.Core.Events;
using Framework.Core.Events.Externals;
using LookUpDomain.Dtos;
using LookUpDomain.Entities;
using LookUpDomain.EventEnvelopes.Attribute;
using LookUpDomain.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LookUpDomain.Services
{
    public interface IAttributeService
    {
        Task<IEnumerable<AttributeDto>> All();
        Task<AttributeDto> AddAttribute(AttributeDto dto);
        Task<AttributeDto> GetAttributeById(Guid id);
        Task<bool> UpdateAttribute(AttributeDtoUpdate dto, Guid id);
        Task<bool> UpdateAttributeStatus(LookUpStatusEnum status, Guid id);
    }
    public class AttributeService : IAttributeService
    {
        private IAttributeRepository _repository;
        public readonly IMapper _mapper;
        private readonly IExternalEventProducer _externalEventProducer;

        public AttributeService(IAttributeRepository repository, IMapper mapper,
            IExternalEventProducer externalEventProducer)
        {
            _repository = repository;
            _mapper = mapper;
            _externalEventProducer = externalEventProducer;
        }

        public async Task<AttributeDto> AddAttribute(AttributeDto dto)
        {
            if (dto != null)
            {
                dto.Status = LookUpStatusEnum.Inactive;
                var dtoToEntity = _mapper.Map<AtrributeEntity>(dto);
                var entity = await _repository.Add(dtoToEntity);
                var result = await _repository.SaveChangesAsync();



                if (result > 0)
                {
                    var externalEvent = new EventEnvelope<AttributeCreated>(
                    AttributeCreated.Create(
                        entity.Id,
                        entity.Type,
                        entity.Unit,
                        entity.Status,
                        entity.Modified
                        )
                    );

                    await _externalEventProducer.Publish(externalEvent, new CancellationToken());
                    return _mapper.Map<AttributeDto>(entity);
                }
            }
            return new AttributeDto();
        }

        public async Task<IEnumerable<AttributeDto>> All()
        {
            return _mapper.Map<IEnumerable<AttributeDto>>(await _repository.GetAll());
        }

        public async Task<AttributeDto> GetAttributeById(Guid id)
        {
            if (id != Guid.Empty)
            {
                var result = await _repository.GetById(id);
                if (result != null)
                {
                    return _mapper.Map<AttributeDto>(result);
                }
            }
            return null;
        }

        public async Task<bool> UpdateAttribute(AttributeDtoUpdate dto, Guid id)
        {
            if (dto != null)
            {
                var attribute = await _repository.GetById(id);
                if (attribute != null)
                {

                    var entity = await _repository.Update(_mapper.Map<AtrributeEntity>(dto));
                    entity.Status = attribute.Status;
                    var result = await _repository.SaveChangesAsync();

                    if (result > 0)
                    {
                        var externalEvent = new EventEnvelope<AttributeUpdated>(
                            AttributeUpdated.Update(
                            entity.Id,
                            entity.Type,
                            entity.Unit,
                            entity.Modified
                            )
                         );

                        await _externalEventProducer.Publish(externalEvent, new CancellationToken());
                        return true;
                    }
                }
            }
            return false;
        }

        public async Task<bool> UpdateAttributeStatus(LookUpStatusEnum status, Guid id)
        {
            if (status != null)
            {
                var attribute = await _repository.GetById(id);
                if (attribute != null)
                {
                    attribute.Status = status;
                    attribute.Modified = DateTime.Now;
                    var entity = await _repository.Update(attribute);
                    var result = await _repository.SaveChangesAsync();

                    if (result > 0)
                    {
                        var externalEvent = new EventEnvelope<AttributeStatusChanged>(
                            AttributeStatusChanged.UpdateStatus(
                                entity.Id,
                                entity.Status,
                                entity.Modified
                                )
                            );

                        await _externalEventProducer.Publish(externalEvent, new CancellationToken());
                        return true;
                    }
                }
            }
            return false;
        }
    }
}

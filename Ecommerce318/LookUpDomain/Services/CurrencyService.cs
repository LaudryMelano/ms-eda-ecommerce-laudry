﻿using AutoMapper;
using LookUpDomain.Dtos;
using LookUpDomain.Entities;
using LookUpDomain.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LookUpDomain.Services
{
    public interface ICurrencyService
    {
        Task<IEnumerable<CurrencyDto>> All();
        Task<CurrencyDto> AddCurrency(CurrencyDto dto);
        Task<CurrencyDto> GetCurrencyById(Guid id);
        Task<bool> UpdateCurrency(CurrencyDtoUpdate dto, Guid id);
        Task<bool> UpdateCurrencyStatus(LookUpStatusEnum status, Guid id);
    }
    public class CurrencyService : ICurrencyService
    {
        private ICurrencyRepository _repository;
        public readonly IMapper _mapper;

        public CurrencyService(ICurrencyRepository repository, IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }

        public async Task<CurrencyDto> AddCurrency(CurrencyDto dto)
        {
            if (dto != null)
            {
                dto.Status = LookUpStatusEnum.Inactive;
                var dtoToEntity = _mapper.Map<CurrencyEntity>(dto);
                var entity = await _repository.Add(dtoToEntity);
                var result = await _repository.SaveChangesAsync();

                if (result > 0)
                {
                    return _mapper.Map<CurrencyDto>(entity);
                }
            }
            return new CurrencyDto();
        }

        public async Task<IEnumerable<CurrencyDto>> All()
        {
            return _mapper.Map<IEnumerable<CurrencyDto>>(await _repository.GetAll());
        }

        public async Task<CurrencyDto> GetCurrencyById(Guid id)
        {
            if (id != Guid.Empty)
            {
                var result = await _repository.GetById(id);
                if (result != null)
                {
                    return _mapper.Map<CurrencyDto>(result);
                }
            }
            return null;
        }

        public async Task<bool> UpdateCurrency(CurrencyDtoUpdate dto, Guid id)
        {
            if (dto != null)
            {
                var currency = await _repository.GetById(id);
                if (currency != null)
                {
                    var entity = await _repository.Update(_mapper.Map<CurrencyEntity>(dto));
                    entity.Status = currency.Status;
                    var result = await _repository.SaveChangesAsync();

                    if (result > 0)
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        public async Task<bool> UpdateCurrencyStatus(LookUpStatusEnum status, Guid id)
        {
            if (status != null)
            {
                var currency = await _repository.GetById(id);
                if (currency != null)
                {
                    currency.Status = status;
                    var entity = await _repository.Update(currency);
                    var result = await _repository.SaveChangesAsync();

                    if (result > 0)
                    {
                        return true;
                    }
                }
            }
            return false;
        }
    }
}

﻿using AutoMapper;
using Cart.Domain.Dtos;
using Cart.Domain.Entities;
using Cart.Domain.EventEnvelopes.Cart;
using Cart.Domain.Repositories;
using Framework.Core.Events;
using Framework.Core.Events.Externals;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cart.Domain.Services
{
    public interface ICartService
    {
        Task<IEnumerable<CartDto>> All();
        Task<IEnumerable<CartEntity>> AllIn();
        Task<CartDto> AddCart(CartDto dto);
        Task<CartDto> GetCartById(Guid id);
        Task<bool> UpdateCartStatus(CartStatusEnum status, Guid id);
    }
    public class CartService : ICartService
    {
        private ICartRepository _repository;
        private ICartProductRepository _repositoryCartProduct;
        public readonly IMapper _mapper;
        private readonly IExternalEventProducer _externalEventProducer;

        public CartService(ICartRepository repository, IMapper mapper, IExternalEventProducer externalEventProducer, ICartProductRepository repositoryCartProduct)
        {
            _repository = repository;
            _mapper = mapper;
            _externalEventProducer = externalEventProducer;
            _repositoryCartProduct = repositoryCartProduct;
        }

        public async Task<CartDto> AddCart(CartDto dto)
        {
            if (dto != null)
            {
                dto.Status = CartStatusEnum.Pending;
                var dtoToEntity = _mapper.Map<CartEntity>(dto);
                var entity = await _repository.Add(dtoToEntity);
                var result = await _repository.SaveChangesAsync();

                if (result > 0)
                {
                    return _mapper.Map<CartDto>(entity);
                }
            }
            return new CartDto();
        }

        public async Task<IEnumerable<CartDto>> All()
        {
            return _mapper.Map<IEnumerable<CartDto>>(await _repository.GetAll());
        }

        public async Task<CartDto> GetCartById(Guid id)
        {
            if (id != Guid.Empty)
            {
                var result = await _repository.GetById(id);
                if (result != null)
                {
                    return _mapper.Map<CartDto>(result);
                }
            }
            return null;
        }

        public async Task<bool> UpdateCartStatus(CartStatusEnum status, Guid id)
        {
            if (id != null)
            {
                var cart = await _repository.GetById(id);
                if (cart != null)
                {
                    cart.Status = status;
                    var entity = await _repository.Update(cart);
                    var result = await _repository.SaveChangesAsync();

                    if (result > 0)
                    {
                        List<CartProductItem> list = new List<CartProductItem>();
                        if (status == CartStatusEnum.Confirmed)
                        {
                            var cartProducts = await _repositoryCartProduct.GetByIdCart(cart.Id);

                            foreach (var item in cartProducts)
                            {
                                list.Add(new CartProductItem()
                                {
                                    Id = item.Id,
                                    ProductId = item.ProductId,
                                    Quantity = item.Quantity
                                });
                            }
                        }

                        var externalEvent = new EventEnvelope<CartStatusChanged>(
                          CartStatusChanged.UpdateStatus(
                              entity.Id,
                              entity.CustomerId,
                              entity.Status,
                              list,
                              entity.Modified
                              )
                          );

                        await _externalEventProducer.Publish(externalEvent, new CancellationToken());

                        return true;
                    }
                }
            }
            return false;
        }

        public async Task<IEnumerable<CartEntity>> AllIn()
        {
            return await _repository.GetAll();
        }
    }
}

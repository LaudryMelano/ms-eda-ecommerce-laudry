﻿using AutoMapper;
using Cart.Domain.Dtos;
using Cart.Domain.Entities;
using Cart.Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cart.Domain.Services
{
    public interface ICartProductService
    {
        Task<IEnumerable<CartProductDto>> All();
        Task<CartProductDto> AddCartProduct(CartProductDto dto);
        Task<CartProductDto> GetCartProductById(Guid id);
        Task<IEnumerable<CartProductDto>> GetCartProductByIdCart(Guid id);
        Task<bool> UpdateCartProduct(CartProductDto dto, Guid id);
    }
    public class CartProductService : ICartProductService
    {
        private ICartProductRepository _repository;
        private IProductRepository _productRepository;
        public readonly IMapper _mapper;

        public CartProductService(ICartProductRepository repository, IMapper mapper, IProductRepository productRepository)
        {
            _repository = repository;
            _mapper = mapper;
            _productRepository = productRepository;
        }

        public async Task<CartProductDto> AddCartProduct(CartProductDto dto)
        {
            var product = await _productRepository.GetById(dto.ProductId);
            if(product == null)
                return null;

            dto.Price = product.Price;
            dto.Name = product.Name;
            dto.SKU = product.SKU;

            var existCp = await _repository.GetByIdCart(dto.CartId, dto.ProductId);
            if (existCp != null)
            {
                if (product.Stock >= dto.Quantity + existCp.Quantity)
                    existCp.Quantity = dto.Quantity + existCp.Quantity;
                else
                    existCp.Quantity = product.Stock;

                var entity = await _repository.Update(existCp);
                var result = await _repository.SaveChangesAsync();

                if (result > 0)
                    return _mapper.Map<CartProductDto>(entity);
            }
            else
            {
                var entity = await _repository.Add(_mapper.Map<CartProductEntity>(dto));
                var result = await _repository.SaveChangesAsync();

                if (result > 0)
                    return _mapper.Map<CartProductDto>(entity);
            }

            return new CartProductDto();
        }

        public async Task<IEnumerable<CartProductDto>> All()
        {
            return _mapper.Map<IEnumerable<CartProductDto>>(await _repository.GetAll());
        }

        public async Task<CartProductDto> GetCartProductById(Guid id)
        {
            if (id != Guid.Empty)
            {
                var result = await _repository.GetById(id);
                if (result != null)
                {
                    return _mapper.Map<CartProductDto>(result);
                }
            }
            return null;
        }

        public async Task<IEnumerable<CartProductDto>> GetCartProductByIdCart(Guid id)
        {
            if (id != Guid.Empty)
            {
                var result = await _repository.GetByIdCart(id);
                if (result != null)
                {                  
                    return _mapper.Map<IEnumerable<CartProductDto>>(result); ;
                }
            }
            return null;
        }

        public async Task<bool> UpdateCartProduct(CartProductDto dto, Guid id)
        {
            if (dto != null)
            {
                var cart = await _repository.GetById(id);
                if (cart != null)
                {
                    var entity = await _repository.Update(_mapper.Map<CartProductEntity>(dto));
                    var result = await _repository.SaveChangesAsync();

                    if (result > 0)
                    {
                        return true;
                    }
                }
            }
            return false;
        }
    }
}

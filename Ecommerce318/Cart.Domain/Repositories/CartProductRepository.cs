﻿using Cart.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cart.Domain.Repositories
{
    public interface ICartProductRepository
    {
        Task<IEnumerable<CartProductEntity>> GetAll();
        Task<CartProductEntity> GetById(Guid id);
        Task<IEnumerable<CartProductEntity>> GetByIdCart(Guid id);
        Task<CartProductEntity> GetByIdCart(Guid id, Guid productId);
        Task<CartProductEntity> Add(CartProductEntity entity);
        Task<CartProductEntity> Update(CartProductEntity entity);
        void Delete(CartProductEntity entity);
        Task<int> SaveChangesAsync(CancellationToken cancellationToken = default);
    }
    public class CartProductRepository : ICartProductRepository
    {
        protected readonly CartDbContext _context;

        public CartProductRepository(CartDbContext context)
        {
            _context = context;
            _context.Database.EnsureCreated();
        }

        public async Task<CartProductEntity> Add(CartProductEntity entity)
        {
            _context.Set<CartProductEntity>().Add(entity);
            return entity;
        }

        public void Delete(CartProductEntity entity)
        {
            _context.Set<CartProductEntity>().Update(entity);
        }

        public async Task<IEnumerable<CartProductEntity>> GetAll()
        {
            return await _context.Set<CartProductEntity>().ToListAsync();
        }

        public async Task<CartProductEntity> GetById(Guid id)
        {
            return await _context.Set<CartProductEntity>().FindAsync(id);
        }

        public async Task<IEnumerable<CartProductEntity>> GetByIdCart(Guid id)
        {
            return await _context.Set<CartProductEntity>().Where(x => x.CartId == id).ToListAsync();
        }

        public async Task<CartProductEntity> GetByIdCart(Guid id, Guid productId)
        {
            return await _context.Set<CartProductEntity>().Where(x => x.CartId == id && x.ProductId == productId).FirstOrDefaultAsync();
        }
        public async Task<int> SaveChangesAsync(CancellationToken cancellationToken = default)
        {
            return await _context.SaveChangesAsync(cancellationToken);
        }

        public async Task<CartProductEntity> Update(CartProductEntity entity)
        {
            _context.Set<CartProductEntity>().Update(entity);
            return entity;
        }

        public virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                _context.Dispose();
            }
        }

        public void Dispose()
        {
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }
    }
}

﻿using Cart.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cart.Domain.Repositories
{
    public interface ICartRepository
    {
        Task<IEnumerable<CartEntity>> GetAll();
        Task<CartEntity> GetById(Guid id);
        Task<CartEntity> Add(CartEntity entity);
        Task<CartEntity> Update(CartEntity entity);
        void Delete(CartEntity entity);
        Task<int> SaveChangesAsync(CancellationToken cancellationToken = default);
    }
    public class CartRepository : ICartRepository
    {
        protected readonly CartDbContext _context;

        public CartRepository(CartDbContext context)
        {
            _context = context;
            _context.Database.EnsureCreated();
        }

        public async Task<CartEntity> Add(CartEntity entity)
        {
            _context.Set<CartEntity>().Add(entity);
            return entity;
        }

        public void Delete(CartEntity entity)
        {
            _context.Set<CartEntity>().Update(entity);
        }

        public async Task<IEnumerable<CartEntity>> GetAll()
        {
            return await _context.Set<CartEntity>().Include(o => o.CartProducts).ToListAsync();
        }

        public async Task<CartEntity> GetById(Guid id)
        {
            return await _context.Set<CartEntity>().FindAsync(id);
        }

        public async Task<int> SaveChangesAsync(CancellationToken cancellationToken = default)
        {
            return await _context.SaveChangesAsync(cancellationToken);
        }

        public async Task<CartEntity> Update(CartEntity entity)
        {
            _context.Set<CartEntity>().Update(entity);
            return entity;
        }

        public virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                _context.Dispose();
            }
        }

        public void Dispose()
        {
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }
    }
}

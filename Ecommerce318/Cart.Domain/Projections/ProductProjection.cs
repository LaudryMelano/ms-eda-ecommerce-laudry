﻿using Cart.Domain.Entities;
using Framework.Core.Events;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cart.Domain.Projections
{
    public record ProductCreated(
        Guid? Id,
        Guid CategoryId,
        Guid AttributeId,
        string SKU,
        string Name,
        string Description,
        decimal Price,
        decimal Volume,
        int Sold,
        int Stock,
        UserStatusEnum Status,
        DateTime Modified
        );

    public record ProductStatusChanged(
        Guid Id,
        UserStatusEnum Status,
        DateTime Modified
        );

    public record ProductUpdated(
        Guid Id,
        Guid CategoryId,
        Guid AttributeId,
        string SKU,
        string Name,
        string Description,
        decimal Price,
        decimal Volume,
        int Sold,
        int Stock,
        DateTime Modified
        );

    public class ProductProjection
    {
        public ProductProjection()
        {

        }
        public static bool Handle(EventEnvelope<ProductCreated> eventEnvelope)
        {
            var (id, categoryId, attributeId, sku, name, description, price, volume, sold, stock, status ,modified) = eventEnvelope.Data;
            using (var context = new CartDbContext(CartDbContext.OnConfigure()))
            {
                ProductEntity entity = new ProductEntity()
                {
                    Id = (Guid)id,
                    CategoryId = categoryId,
                    AttributeId = attributeId,
                    SKU = sku,
                    Name = name,
                    Description = description,
                    Price = price,
                    Volume = volume,
                    Sold = sold,
                    Stock = stock,
                    Status = status,
                    Modified = modified
                };

                context.Products.Add(entity);
                context.SaveChanges();
            }


            return true; //new AttributeCreated(id, type, unit, status, modified);
        }

        public static bool Handle(EventEnvelope<ProductUpdated> eventEnvelope)
        {
            var (id, categoryId, attributeId, sku, name, description, price, volume, sold, stock, modified) = eventEnvelope.Data;
            using (var context = new CartDbContext(CartDbContext.OnConfigure()))
            {
                ProductEntity entity = context.Set<ProductEntity>().Find(id);
                entity.Id = id;
                entity.CategoryId = categoryId;
                entity.AttributeId = attributeId;
                entity.SKU = sku;
                entity.Name = name;
                entity.Description = description;
                entity.Price = price;
                entity.Volume = volume;
                entity.Sold = sold;
                entity.Stock = stock;
                entity.Modified = modified;

                context.SaveChanges();
            }

            return true; //new AttributeCreated(id, type, unit, status, modified);
        }

        public static bool Handle(EventEnvelope<ProductStatusChanged> eventEnvelope)
        {
            var (id, status, modified) = eventEnvelope.Data;
            using (var context = new CartDbContext(CartDbContext.OnConfigure()))
            {
                ProductEntity entity = context.Set<ProductEntity>().Find(id);
                entity.Status = status;
                entity.Modified = modified;
                context.SaveChanges();
            }

            return true; //new AttributeCreated(id, type, unit, status, modified);
        }
    }
}

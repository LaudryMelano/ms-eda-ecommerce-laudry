﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cart.Domain.EventEnvelopes.Cart
{
    public record CartStatusChanged (
        Guid Id,
        Guid CustomerId,
        CartStatusEnum Status,
        List<CartProductItem> CartProducts,
        DateTime Modified
        )
    {
        public static CartStatusChanged UpdateStatus(
            Guid id,
            Guid customerId,
            CartStatusEnum status,
            List<CartProductItem> cartProducts,
            DateTime modified
            ) => new(id, customerId, status, cartProducts, modified);
    }

    public class CartProductItem
    {
        public Guid Id { get; set; }
        public Guid ProductId { get; set; }
        public int Quantity { get; set; }
    }
}

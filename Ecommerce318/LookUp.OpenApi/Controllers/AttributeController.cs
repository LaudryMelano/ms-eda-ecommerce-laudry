﻿using FluentValidation;
using FluentValidation.Results;
using LookUpDomain;
using LookUpDomain.Dtos;
using LookUpDomain.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace LookUp.OpenApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AttributeController : ControllerBase
    {
        private readonly IAttributeService _service;
        private IValidator<AttributeDto> _validator;
        private IValidator<AttributeDtoUpdate> _validatorUpdate;
        private readonly ILogger<AttributeController> _logger;

        public AttributeController(IAttributeService service, IValidator<AttributeDto> validator, IValidator<AttributeDtoUpdate> validatorUpdate, ILogger<AttributeController> logger)
        {
            _service = service;
            _logger = logger;
            _validator = validator;
            _validatorUpdate = validatorUpdate;
        }

        [HttpGet("GetAllAttribute")]
        public async Task<IActionResult> Get() 
        {

            return Ok( await _service.All());
        }

        [HttpPost("PostAttribute")]
        public async Task<IActionResult> Post([FromBody] AttributeDto payload, CancellationToken
            cancellationToken)
        {
            try
            {
                ValidationResult result = await _validator.ValidateAsync(payload);
                if (!result.IsValid)
                    return BadRequest(result);

                var dto = await _service.AddAttribute(payload);
                if (dto != null)
                {
                    return Ok(dto);
                }
            }
            catch (OperationCanceledException ex) when (cancellationToken.IsCancellationRequested)
            {
                _logger.LogWarning(ex.Message);
                throw;
            }
            return BadRequest();
        }

        [HttpPut("EditAttribute")]
        public async Task<IActionResult> Put([FromBody] AttributeDtoUpdate payload,Guid id, CancellationToken cancellationToken)
        {
            try
            {
                if (payload != null)
                {
                    ValidationResult result = await _validatorUpdate.ValidateAsync(payload);
                    if (!result.IsValid)
                        return BadRequest(result);

                    payload.Id = id;
                    var isUpdate = await _service.UpdateAttribute(payload, id);
                    if(isUpdate)
                        return Ok(isUpdate);
                }
            }
            catch (OperationCanceledException ex) when (cancellationToken.IsCancellationRequested)
            {
                _logger.LogWarning(ex.Message);
                throw;
            }
            return NoContent();
        }

        [HttpPut("EditAttributeStatus")]
        public async Task<IActionResult> Put( Guid id, LookUpStatusEnum status, CancellationToken cancellationToken)
        {
            try
            {
                if (status != null)
                {
                    var isUpdate = await _service.UpdateAttributeStatus(status, id);
                    if (isUpdate)
                        return Ok(isUpdate);
                }
            }
            catch (OperationCanceledException ex) when (cancellationToken.IsCancellationRequested)
            {
                _logger.LogWarning(ex.Message);
                throw;
            }
            return NoContent();
        }
    }
}

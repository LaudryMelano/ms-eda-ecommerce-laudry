﻿using LookUpDomain.Dtos;
using LookUpDomain.Services;
using LookUpDomain;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using FluentValidation;
using FluentValidation.Results;

namespace LookUp.OpenApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CurrencyController : ControllerBase
    {
        private readonly ICurrencyService _service;
        private IValidator<CurrencyDto> _validator;
        private readonly ILogger<CurrencyController> _logger;


        public CurrencyController(ICurrencyService service, IValidator<CurrencyDto> validator, ILogger<CurrencyController> logger)
        {
            _service = service;
            _logger = logger;
            _validator = validator;
        }

        [HttpGet("GetAllCurrency")]
        public async Task<IActionResult> Get()
        {

            return Ok(await _service.All());
        }

        [HttpPost("PostCurrency")]
        public async Task<IActionResult> Post([FromBody] CurrencyDto payload, CancellationToken
            cancellationToken)
        {
            try
            {
                ValidationResult result = await _validator.ValidateAsync(payload);
                if (!result.IsValid)
                    return BadRequest(result);

                var dto = await _service.AddCurrency(payload);
                if (dto != null)
                {
                    return Ok(dto);
                }
            }
            catch (OperationCanceledException ex) when (cancellationToken.IsCancellationRequested)
            {
                _logger.LogWarning(ex.Message);
                throw;
            }
            return BadRequest();
        }

        [HttpPut("EditCurrency")]
        public async Task<IActionResult> Put([FromBody] CurrencyDtoUpdate payload, Guid id, CancellationToken cancellationToken)
        {
            try
            {
                if (payload != null)
                {
                    payload.Id = id;
                    var isUpdate = await _service.UpdateCurrency(payload, id);
                    if (isUpdate)
                        return Ok(isUpdate);
                }
            }
            catch (OperationCanceledException ex) when (cancellationToken.IsCancellationRequested)
            {
                _logger.LogWarning(ex.Message);
                throw;
            }
            return NoContent();
        }

        [HttpPut("EditCurrencyStatus")]
        public async Task<IActionResult> Put(Guid id, LookUpStatusEnum status, CancellationToken cancellationToken)
        {
            try
            {
                if (status != null)
                {
                    var isUpdate = await _service.UpdateCurrencyStatus(status, id);
                    if (isUpdate)
                        return Ok(isUpdate);
                }
            }
            catch (OperationCanceledException ex) when (cancellationToken.IsCancellationRequested)
            {
                _logger.LogWarning(ex.Message);
                throw;
            }
            return NoContent();
        }
    }
}

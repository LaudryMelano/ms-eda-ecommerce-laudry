using Framework.Core.Events;
using Framework.Kafka;
using Microsoft.EntityFrameworkCore;
using Payment.Domain;
using Payment.Domain.MapProfile;
using Payment.Domain.Repositories;
using Payment.Domain.Services;
using Payment.GraphQL.Schema.Mutations;
using Payment.GraphQL.Schema.Queries;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.



builder.Services.AddDomainContext(options =>
{
    var builder = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile
    ("appsettings.json", optional: true, reloadOnChange: true);

    options.UseSqlServer(builder.Build().GetSection("ConnectionStrings").GetSection("Payment_Db_Conn").Value);

    options.EnableSensitiveDataLogging(false).UseQueryTrackingBehavior(QueryTrackingBehavior.NoTracking);
});

builder.Services.AddAutoMapper(config =>
{
    config.AddProfile<EntityToDtoProfile>();
});

builder.Services
.AddScoped<Query>()
.AddScoped<PaymentQuery>()
.AddScoped<Mutation>()
.AddScoped<PaymentMutation>()
.AddScoped<ICartProductRepository, CartProductRepository>()
.AddScoped<IProductRepository, ProductRepository>()
.AddScoped<IPaymentRepository, PaymentRepository>()
.AddScoped<IPaymentService, PaymentService>()
.AddGraphQLServer()
.AddQueryType<Query>()
.AddTypeExtension<PaymentQuery>()
.AddMutationType<Mutation>()
.AddTypeExtension<PaymentMutation>(); 

builder.Services.AddPayment();
builder.Services.AddEventBus();
builder.Services.AddKafkaProducerAndConsumer();


builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.MapGraphQL();

app.Run();

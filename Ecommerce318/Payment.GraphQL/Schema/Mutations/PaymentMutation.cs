﻿using Payment.Domain.Dtos;
using Payment.Domain.Services;

namespace Payment.GraphQL.Schema.Mutations
{
    [ExtendObjectType(typeof(Mutation))]
    public class PaymentMutation
    {
        private readonly IPaymentService _service;

        public PaymentMutation(IPaymentService service)
        {
            _service = service;
        }

        public async Task<PaymentDto> Payment(Guid id, decimal mount)
        {
            return await _service.Paying(id, mount);
        }
    }
}

﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Payment.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Payment.Domain
{
    public static class ServiceExtension
    {
        public static void AddDomainContext(this IServiceCollection services,
         Action<DbContextOptionsBuilder> optionAction,
         ServiceLifetime contextLifeTime = ServiceLifetime.Scoped,
         ServiceLifetime optionLifeTime = ServiceLifetime.Scoped)
        {
            services.AddDbContext<PaymentDbContext>(optionAction, contextLifeTime, optionLifeTime);
        }
    }
}

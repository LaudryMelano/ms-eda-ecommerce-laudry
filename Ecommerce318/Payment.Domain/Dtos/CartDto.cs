﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Payment.Domain.Dtos
{
    public class CartDto
    {
        public Guid Id { get; set; }
        public Guid CartId { get; set; }
        public Decimal Total { get; set; }
        public Decimal Pay { get; set; }
        public CartStatusEnum CartStatus { get; internal set; } 
        public PaymentStatusEnum Status { get; internal set; }
        public UserDto? Customer { get; set; }
    }

    public class UserDto
    {
        public Guid Id { get; set; }
        public string UserName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
    }
}

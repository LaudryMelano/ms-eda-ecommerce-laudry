﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Payment.Domain.Dtos
{
    public class PaymentDto
    {
        public Guid Id { get; set; }
        public Guid CartId { get; set; }
        public Decimal Total { get; set; }
        public Decimal Pay { get; set; }
        public CartStatusEnum CartStatus { get; internal set; } 
        public PaymentStatusEnum Status { get; internal set; } 
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Payment.Domain.Entities
{
    public class PaymentEntity
    {
        public Guid Id { get; set; }
        public Guid CartId { get; set; }
        public Decimal Total { get; set; }
        public Decimal? Pay { get; set; }
        public CartStatusEnum CartStatus { get; set; } = default;
        public PaymentStatusEnum Status { get; set; } = default;
        public DateTime Modified { get; internal set; } = DateTime.Now;

        //Relationship
        [ForeignKey("CartId")]
        public virtual CartEntity Cart { get; set; }
    }
}

﻿using Microsoft.EntityFrameworkCore;
using Payment.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Payment.Domain.Repositories
{
    public interface IPaymentRepository
    {
        Task<IEnumerable<PaymentEntity>> GetAll();
        Task<PaymentEntity> GetById(Guid id);
        Task<PaymentEntity> Add(PaymentEntity entity);
        Task<PaymentEntity> Update(PaymentEntity entity);
        Task<int> SaveChangesAsync(CancellationToken cancellationToken = default);
    }
    public class PaymentRepository : IPaymentRepository
    {
        protected readonly PaymentDbContext _context;

        public PaymentRepository(PaymentDbContext context)
        {
            _context = context;
            _context.Database.EnsureCreated();
        }

        public async Task<PaymentEntity> Add(PaymentEntity entity)
        {
            _context.Set<PaymentEntity>().Add(entity);
            return entity;
        }

        public async Task<IEnumerable<PaymentEntity>> GetAll()
        {
            return await _context.Set<PaymentEntity>().ToArrayAsync();
        }

        public async Task<PaymentEntity> GetById(Guid id)
        {
            return await _context.Set<PaymentEntity>().FindAsync(id);
        }

        public async Task<int> SaveChangesAsync(CancellationToken cancellationToken = default)
        {
            return await _context.SaveChangesAsync(cancellationToken);
        }

        public async Task<PaymentEntity> Update(PaymentEntity entity)
        {
            _context.Set<PaymentEntity>().Update(entity);
            return entity;
        }

        public virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                _context.Dispose();
            }
        }

        public void Dispose()
        {
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }
    }
}

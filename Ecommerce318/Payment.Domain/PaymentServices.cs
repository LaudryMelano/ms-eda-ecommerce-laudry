﻿using Framework.Core.Projection;
using Microsoft.Extensions.DependencyInjection;
using Payment.Domain.Projections;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Payment.Domain
{
    public static class PaymentServices
    {
        public static IServiceCollection AddPayment(this IServiceCollection services) =>
            services
                .Projection(builder => builder
                    .AddOn<UserCreated>(UserProjection.Handle)
                    .AddOn<ProductCreated>(ProductProjection.Handle)
                    .AddOn<CartStatusChanged>(CartProjection.Handle)
            );
    }
}

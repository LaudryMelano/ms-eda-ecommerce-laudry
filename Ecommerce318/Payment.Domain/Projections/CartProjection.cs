﻿using AutoMapper;
using Framework.Core.Events;
using Payment.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Payment.Domain.Projections
{
    public record CartStatusChanged(
        Guid Id,
        Guid CustomerId,
        List<CartProductItem> CartProducts,
        CartStatusEnum Status
    );

    public class CartProductItem
    {
        public Guid Id { get; set; }
        public Guid ProductId { get; set; }
        public int Quantity { get; set; }
    }

    public class CartProjection
    {
        public static bool Handle(EventEnvelope<CartStatusChanged> eventEnvelope)
        {
            
            var (id, customerId, cartProducts, status) = eventEnvelope.Data;
            using (var context = new PaymentDbContext(PaymentDbContext.OnConfigure()))
            {
                CartEntity newEntity = new CartEntity()
                {
                    Id = (Guid)id,
                    CustomerId = customerId,
                    Status = status
                };
                context.Carts.Add(newEntity);

                if (newEntity != null)
                {
                    decimal total = 0;

                    foreach (var item in cartProducts)
                    {
                        ProductEntity prod = context.Products.Where(o => o.Id == item.ProductId).FirstOrDefault()!;
                        if (prod != null)
                            total += item.Quantity * prod.Price;

                        CartProductEntity cartProd = new CartProductEntity()
                        {
                            Id = (Guid)item.Id,
                            CartId = id,
                            ProductId = item.ProductId,
                            Quantity = item.Quantity
                        };
                        context.CartsProducts.Add(cartProd);
                        context.SaveChanges();
                    }

                    PaymentEntity entityPayment = new PaymentEntity();
                    entityPayment.Id = new Guid();
                    entityPayment.CartId = id;
                    entityPayment.CartStatus = status;
                    entityPayment.Total = total;
                    entityPayment.Pay = 0;
                    entityPayment.Status = PaymentStatusEnum.Pending;
                    context.Payments.Add(entityPayment);

                    context.SaveChanges();
                }
                
            }
            return true;
        }
    }
}

﻿using Framework.Core.Events;
using Payment.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Payment.Domain.Projections
{
    public record ProductCreated(
       Guid? Id,
       decimal Price,
       int Stock,
       StoreStatusEnum Status,
       DateTime Modified
       );

    public record ProductStatusChanged(
        Guid Id,
        StoreStatusEnum Status,
        DateTime Modified
        );

    public class ProductProjection
    {
        public static bool Handle(EventEnvelope<ProductCreated> eventEnvelope)
        {
            var (id,  price, stock, status, modified) = eventEnvelope.Data;
            using (var context = new PaymentDbContext(PaymentDbContext.OnConfigure()))
            {
                ProductEntity entity = new ProductEntity()
                {
                    Id = (Guid)id,
                    Price = price,
                    Stock = stock,
                    Status = status,
                };

                context.Products.Add(entity);
                context.SaveChanges();
            }
            return true; 
        }

        public static bool Handle(EventEnvelope<ProductStatusChanged> eventEnvelope)
        {
            var (id, status, modified) = eventEnvelope.Data;
            using (var context = new PaymentDbContext(PaymentDbContext.OnConfigure()))
            {
                ProductEntity entity = context.Set<ProductEntity>().Find(id);
                entity.Status = status;
                context.SaveChanges();
            }

            return true;
        }
    }
}

﻿using AutoMapper;
using Framework.Core.Events;
using Framework.Core.Events.Externals;
using Payment.Domain.Dtos;
using Payment.Domain.Entities;
using Payment.Domain.EventEnvelopes;
using Payment.Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Payment.Domain.Services
{
    public interface IPaymentService
    {
        Task<IEnumerable<PaymentDto>> All();
        Task<PaymentDto> Paying(Guid id, decimal amount);
        Task<PaymentDto> GetPaymentById(Guid id);
        Task<bool> PaymentCancel(PaymentStatusEnum status, Guid id);
    }
    public class PaymentService : IPaymentService
    {
        private IPaymentRepository _repository;
        private readonly ICartProductRepository _cartProductRepository;
        private readonly IProductRepository _productRepository;
        public readonly IMapper _mapper;
        private readonly IExternalEventProducer _externalEventProducer;
        public PaymentService(IPaymentRepository repository, ICartProductRepository cartProductRepository, IProductRepository productRepository, IMapper mapper, IExternalEventProducer externalEventProducer) 
        {
            _repository = repository;
            _cartProductRepository = cartProductRepository;
            _productRepository = productRepository;
            _mapper = mapper;
            _externalEventProducer = externalEventProducer;
        }

        public async Task<IEnumerable<PaymentDto>> All()
        {
            return _mapper.Map<IEnumerable<PaymentDto>>(await _repository.GetAll());
        }

        public Task<PaymentDto> GetPaymentById(Guid id)
        {
            throw new NotImplementedException();
        }

        public Task<bool> PaymentCancel(PaymentStatusEnum status, Guid id)
        {
            throw new NotImplementedException();
        }

        public async Task<PaymentDto> Paying(Guid id, decimal amount)
        {
            PaymentEntity entity =  await _repository.GetById(id);

            if(entity.Status == PaymentStatusEnum.Pending)
            {
                if (entity.Total <= amount)
                {
                   
                    var list = await _cartProductRepository.GetByCartId(entity.CartId);

                    foreach (var item in list)
                    {
                        var product = await _productRepository.GetById(item.ProductId);
                        if (product != null)
                        {
                            if(product.Stock < item.Quantity || product.Status != StoreStatusEnum.Active) 
                            {
                                return null;
                            }
                        }
                        else
                        {
                            return null;
                        }
                    }

                    List<CartProductItem> cartProducts = _mapper.Map<IEnumerable<CartProductItem>>(list).ToList();

                    foreach (var item in list)
                    {
                        var product = await _productRepository.GetById(item.ProductId);
                        if (product != null)
                        {
                            product.Stock = product.Stock - item.Quantity;

                            var selected = cartProducts.Where(o => o.ProductId == item.ProductId).FirstOrDefault();

                            if (selected != null)
                                selected.Stock = product.Stock;

                            await _productRepository.Update(product);
                        }
                        else
                        {
                            return null;
                        }
                    }

                    entity.Pay = amount;
                    entity.Status = PaymentStatusEnum.Paid;
                    entity.CartStatus = CartStatusEnum.Paid;

                    await _repository.Update(entity);
                    var result = await _repository.SaveChangesAsync();

                    if (result > 0)
                    {
                        var externalEvent = new EventEnvelope<PaymentCreated>(
                            PaymentCreated.Create(
                                entity.CartId,
                                cartProducts,
                                entity.CartStatus
                            )
                        );

                        await _externalEventProducer.Publish(externalEvent, new CancellationToken());

                        return _mapper.Map<PaymentDto>(entity);
                    }
                }
                return null;
            }
            return null;
        }
    }
}

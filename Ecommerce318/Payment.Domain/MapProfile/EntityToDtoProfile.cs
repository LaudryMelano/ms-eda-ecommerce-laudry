﻿using AutoMapper;
using Payment.Domain.Dtos;
using Payment.Domain.Entities;
using Payment.Domain.EventEnvelopes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Payment.Domain.MapProfile
{
    public class EntityToDtoProfile : Profile
    {
        public EntityToDtoProfile() : base("Entity To Dto Profile")
        {
            CreateMap<PaymentEntity, PaymentDto>();
            CreateMap<PaymentDto, PaymentEntity>();

            CreateMap<CartEntity, CartDto>();
            CreateMap<CartDto, CartEntity>();

            CreateMap<CartProductEntity, CartProductItem>();
            CreateMap<CartProductItem, CartProductEntity>();
        }
    }
}

﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using User.Domain.Dtos;
using User.Domain.Entities;

namespace User.Domain.MapProfile
{
    public class EntityToDtoProfile: Profile
    {
        public EntityToDtoProfile(): base("Entity to Dtos Profile")
        {
            CreateMap<UserEntity, UserDto>();
            CreateMap<UserDto, UserEntity>();

            CreateMap<UserEntity, LoginDto>();
            CreateMap<LoginDto, UserEntity>();

            CreateMap<UserEntity, UserDtoUpdate>();
            CreateMap<UserDtoUpdate, UserEntity>();

        }
    }
}

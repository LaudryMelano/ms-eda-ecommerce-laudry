﻿using AutoMapper;
using Framework.Auth;
using Framework.Core.Events;
using Framework.Core.Events.Externals;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using User.Domain.Dtos;
using User.Domain.Entities;
using User.Domain.EventEnvelopes;
using User.Domain.Repositories;

namespace User.Domain.Services
{
    public interface IUserService
    {
        Task<IEnumerable<UserDto>> All();

        Task<LoginDto> Login(string userName, string password);
        Task<UserDto> AddUser(UserDto dto);
        Task<UserDto> GetUserById(Guid id);
        Task<bool> UpdateUser(UserDtoUpdate dto, Guid id);
        Task<bool> UpdateUserStatus(UserStatusEnum status, Guid id);
        Task<bool> UpdateUserType(UserTypeEnum type, Guid id);
        Task<bool> DeleteUser(Guid id);
    }
    public class UserService : IUserService
    {
        private IUserRepository _repository;
        public readonly IMapper _mapper;
        private readonly IExternalEventProducer _externalEventProducer;

        public UserService(IUserRepository repository, IMapper mapper, IExternalEventProducer externalEventProducer)
        {
            _repository = repository;
            _mapper = mapper;
            _externalEventProducer = externalEventProducer;
        }

        public async Task<UserDto> AddUser(UserDto dto)
        {
            if (dto != null)
            {
                dto.Status = UserStatusEnum.Inactive;
                dto.Type = UserTypeEnum.Customer;
                dto.Password = Encryption.HashSha256(dto.Password);
                var dtoToEntity = _mapper.Map<UserEntity>(dto);
                var entity = await _repository.Add(dtoToEntity);
                var result = await _repository.SaveChangesAsync();

                if (result > 0)
                {
                    var externalEvent = new EventEnvelope<UserCreated>(
                    UserCreated.Create(
                            entity.Id,
                            entity.UserName,
                            entity.Password,
                            entity.FirstName,
                            entity.LastName,
                            entity.Email,
                            entity.Type,
                            entity.Status,
                            entity.Modified
                        )
                    );

                    await _externalEventProducer.Publish(externalEvent, new CancellationToken());
                    return _mapper.Map<UserDto>(entity);
                }
            }
            return new UserDto();
        }

        public async Task<IEnumerable<UserDto>> All()
        {
            return _mapper.Map<IEnumerable<UserDto>>(await _repository.GetAll());
        }

        public Task<bool> DeleteUser(Guid id)
        {
            throw new NotImplementedException();
        }

        public async Task<UserDto> GetUserById(Guid id)
        {
            if (id != Guid.Empty)
            {
                var result = await _repository.GetById(id);
                if (result != null)
                {
                    return _mapper.Map<UserDto>(result);
                }
            }
            return null;
        }

        public async Task<bool> UpdateUserType(UserTypeEnum type, Guid id)
        {
            if (id != null)
            {
                var user = await _repository.GetById(id);
                if (user != null)
                {
                    user.Type = type;
                    user.Modified = DateTime.Now;
                    var entity = await _repository.Update(user);
                    var result = await _repository.SaveChangesAsync();

                    if (result > 0)
                    {
                        var externalEvent = new EventEnvelope<UserStatusChanged>(
                            UserStatusChanged.UpdateStatus(
                                entity.Id,
                                entity.Status,
                                entity.Modified
                                )
                            );

                        await _externalEventProducer.Publish(externalEvent, new CancellationToken());
                        return true;
                    }
                }
            }
            return false;
        }

        public async Task<bool> UpdateUser(UserDtoUpdate dto, Guid id)
        {
            if (dto != null)
            {
                var user = await _repository.GetById(id);
                if (user != null)
                {

                    var entity = await _repository.Update(_mapper.Map<UserEntity>(dto));
                    entity.Type = user.Type;
                    entity.Status = user.Status;
                    var result = await _repository.SaveChangesAsync();

                    if (result > 0)
                    {
                        var externalEvent = new EventEnvelope<UserUpdated>(
                            UserUpdated.Update(
                            entity.Id,
                            entity.UserName,
                            entity.Password,
                            entity.FirstName,
                            entity.LastName,
                            entity.Email,
                            entity.Modified
                            )
                         );

                        await _externalEventProducer.Publish(externalEvent, new CancellationToken());
                        return true;
                    }
                }
            }
            return false;
        }

        public async Task<bool> UpdateUserStatus(UserStatusEnum status, Guid id)
        {
            if (id != null)
            {
                var user = await _repository.GetById(id);
                if (user != null)
                {
                    user.Status = status;
                    user.Modified = DateTime.Now;
                    var entity = await _repository.Update(user);
                    var result = await _repository.SaveChangesAsync();

                    if (result > 0)
                    {
                        var externalEvent = new EventEnvelope<UserStatusChanged>(
                            UserStatusChanged.UpdateStatus(
                                entity.Id,
                                entity.Status,
                                entity.Modified
                                )
                            );

                        await _externalEventProducer.Publish(externalEvent, new CancellationToken());
                        return true;
                    }
                }
            }
            return false;
        }

        public async Task<LoginDto> Login(string userName, string password)
        {
            var entity = await _repository.Login(userName, password);
            if(entity != null)
            {
                LoginDto dto = _mapper.Map<LoginDto>(entity);
                dto.Roles.Add(entity.Type.ToString());
                return dto;
            }
            return null;
        }
    }
}

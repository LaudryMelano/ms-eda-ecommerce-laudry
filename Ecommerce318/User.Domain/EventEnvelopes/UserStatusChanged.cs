﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace User.Domain.EventEnvelopes
{
    public record UserStatusChanged(
        Guid Id,
        UserStatusEnum Status,
        DateTime Modified
        )
    {
        public static UserStatusChanged UpdateStatus(
            Guid id,
            UserStatusEnum status,
            DateTime modified
            ) => new(id, status, modified);
    }
}

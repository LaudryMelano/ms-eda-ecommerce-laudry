﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace User.Domain.EventEnvelopes
{
    public record UserTypeChanged(
        Guid Id,
        UserTypeEnum Type,
        DateTime Modified
        )
    {
        public static UserTypeChanged UpdateType(
            Guid id,
            UserTypeEnum type,
            DateTime modified
            ) => new(id, type, modified);
    }
}

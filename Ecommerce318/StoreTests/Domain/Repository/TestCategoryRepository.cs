﻿using FluentAssertions;
using Microsoft.EntityFrameworkCore;
using Store.Domain.Entities;
using Store.Domain.Repositories;
using StoreTests.MockData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit.Abstractions;

namespace StoreTests.Domain.Repository
{
    public class TestCategoryRepository : IDisposable
    {
        protected readonly StoreDbContext _context;
        private readonly ITestOutputHelper _output;

        public TestCategoryRepository(ITestOutputHelper output)
        {
            var options = new DbContextOptionsBuilder<StoreDbContext>()
                .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString())
                .Options;

            _context = new StoreDbContext(options);
            _context.Database.EnsureCreated();

            _output = output;
        }

        [Fact]
        public async Task GetAllAsync_ReturnCategoriesCollection()
        {
            //Range
            _context.Categories.AddRange(CategoryMockData.GetCategories());
            _context.SaveChanges();

            var repo = new CategoryRepository(_context);
            //Action
            var result = await repo.GetAll();
            var count = CategoryMockData.GetCategories().Count();
            _output.WriteLine("Result : {0}", count);
            //Resert
            result.Should().HaveCount(2);
        }

        [Fact]
        public async Task GetCategoryAsync_ReturnName()
        {
            //Range
            _context.Categories.AddRange(CategoryMockData.GetCategories());
            _context.SaveChanges();

            var repo = new CategoryRepository(_context);
            //Action
            var result = await repo.GetById(new Guid("7781766E-B11A-42EE-9D89-9132BB370D64"));
            _output.WriteLine("Result : {0}", result.Name.ToString());
            //Resert
            result.Name.Should().Be("Food");
        }

        public void Dispose()
        {
            _context.Database.EnsureDeleted();
            _context.Dispose();
        }
    }
}

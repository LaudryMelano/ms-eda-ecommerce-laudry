﻿using Store.Domain;
using Store.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StoreTests.MockData
{
    public class CategoryMockData
    {
        public static List<CategoryEntity> GetCategories() 
        { 
            return new List<CategoryEntity>
            {
                new CategoryEntity
                {
                    Id = new Guid("7781766E-B11A-42EE-9D89-9132BB370D64"),
                    Name = "Food",
                    Description = "Main Course",
                    Status = StoreStatusEnum.Active
                },
                 new CategoryEntity
                {
                    Id = new Guid("FE14B11E-7FDF-4F03-9787-F0C594267151"),
                    Name = "Drink",
                    Description = "Soft Drink",
                    Status = StoreStatusEnum.Inactive
                }
            }; 
        }

    }
}

﻿using HotChocolate.Authorization;
using LookUpDomain;
using LookUpDomain.Dtos;
using LookUpDomain.Services;
using System.Data;

namespace LookUp.GraphQL.Schema.Mutations
{
    [ExtendObjectType(Name = "Mutation")]
    public class AttributeMutation
    {
        private readonly IAttributeService _service;

        public AttributeMutation(IAttributeService service)
        {
            _service = service;
        }

        [Authorize(Roles = new[] { "admin" })]
        public async Task<AttributeDto> addAttribute(AttributeTypeInput input)
        {
            AttributeDto dto = new AttributeDto();
            dto.Type = input.Type;
            dto.Unit = input.Unit;
            var result = await _service.AddAttribute(dto);

            return result;
        }

        [Authorize(Roles = new[] { "admin" })]
        public async Task<AttributeDtoUpdate> updateAttribute(AttributeTypeInput input, Guid id)
        {
            AttributeDtoUpdate dto = new AttributeDtoUpdate();
            dto.Id = id;
            dto.Type = input.Type;
            dto.Unit = input.Unit;
            var result = await _service.UpdateAttribute(dto, id);
            if (!result)
            {
                throw new GraphQLException(new Error("Attribute not Found", 
                    "404"));
            }
            return dto;
        }

        [Authorize(Roles = new[] { "admin" })]
        public async Task<AttributeDto> updateAttributeStatus(LookUpStatusEnum status, Guid id)
        {
            var result = await _service.UpdateAttributeStatus(status, id);
            if(!result)
            {
                throw new GraphQLException(new Error("Attribute not found", "404"));
            }
            AttributeDto dto = await _service.GetAttributeById(id);
            return dto;
        }
    }
}

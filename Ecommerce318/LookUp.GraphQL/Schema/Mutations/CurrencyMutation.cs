﻿using HotChocolate.Authorization;
using LookUpDomain;
using LookUpDomain.Dtos;
using LookUpDomain.Services;
using System.Data;

namespace LookUp.GraphQL.Schema.Mutations
{
    [ExtendObjectType(Name = "Mutation")]
    public class CurrencyMutation
    {
        private readonly ICurrencyService _service;

        public CurrencyMutation(ICurrencyService service)
        {
            _service = service;
        }

        [Authorize(Roles = new[] { "admin" })]
        public async Task<CurrencyDto> addCurrency(CurrencyTypeInput input)
        {
            CurrencyDto dto = new CurrencyDto();
            dto.Name= input.Name;
            dto.Code= input.Code;
            dto.Symbol= input.Symbol;

            var result = await _service.AddCurrency(dto);

            return result;
        }

        [Authorize(Roles = new[] { "admin" })]
        public async Task<CurrencyDtoUpdate> updateCurrency(CurrencyTypeInput input, Guid id)
        {
            CurrencyDtoUpdate dto = new CurrencyDtoUpdate();
            dto.Id = id;
            dto.Name= input.Name;
            dto.Code= input.Code;
            dto.Symbol= input.Symbol;
            var result = await _service.UpdateCurrency(dto, id);
            if (!result)
            {
                throw new GraphQLException(new Error("Currency not Found",
                    "404"));
            }
            return dto;
        }

        [Authorize(Roles = new[] { "admin" })]
        public async Task<CurrencyDto> updateCurrencyStatus(LookUpStatusEnum status, Guid id)
        {
            var result = await _service.UpdateCurrencyStatus(status, id);
            if (!result)
            {
                throw new GraphQLException(new Error("Currency not found", "404"));
            }
            CurrencyDto dto = await _service.GetCurrencyById(id);
            return dto;
        }
    }
}

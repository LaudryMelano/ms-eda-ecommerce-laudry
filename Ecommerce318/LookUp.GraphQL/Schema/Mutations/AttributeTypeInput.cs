﻿using LookUpDomain;

namespace LookUp.GraphQL.Schema.Mutations
{
    public class AttributeTypeInput
    {
        public AttributeTypeEnum Type { get; set; }
        public string Unit { get; set; }
    }
}

﻿using HotChocolate.Authorization;
using LookUpDomain.Dtos;
using LookUpDomain.Services;
using Microsoft.AspNetCore.Mvc;
using System.Data;

namespace LookUp.GraphQL.Schema.Queries
{

    [ExtendObjectType(Name = "Query")]
    public class AttributeQuery 
    {
        private readonly IAttributeService _service;

        public AttributeQuery(IAttributeService service)
        {
            _service = service;
        }

        [Authorize(Roles = new[] { "admin" })]
        public async Task<IEnumerable<AttributeDto>> gettAllAttribute()
        {
            IEnumerable<AttributeDto> result = await _service.All();
            return result;
        }

        [Authorize(Roles = new[] { "admin" })]
        public async Task<AttributeDto> getAttributeById(Guid id)
        {
            AttributeDto result = await _service.GetAttributeById(id);
            return result;
        }

    }
}

﻿using HotChocolate.Authorization;
using LookUpDomain.Dtos;
using LookUpDomain.Services;
using System.Data;

namespace LookUp.GraphQL.Schema.Queries
{
    [ExtendObjectType(Name = "Query")]
    public class CurrencyQuery
    {
        private readonly ICurrencyService _service;

        public CurrencyQuery(ICurrencyService service)
        {
            _service = service;
        }
        [Authorize(Roles = new[] { "admin" })]
        public async Task<IEnumerable<CurrencyDto>> getAllCurrency()
        {
            IEnumerable<CurrencyDto> result = await _service.All();
            return result;
        }

        [Authorize(Roles = new[] { "admin" })]
        public async Task<CurrencyDto> getCurrencyById(Guid id)
        {
            return await _service.GetCurrencyById(id);
        }
    }
}

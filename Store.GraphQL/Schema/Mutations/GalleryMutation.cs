﻿using Store.Domain.Dtos;
using Store.Domain.Services;
using Path = System.IO.Path;

namespace Store.GraphQL.Schema.Mutations
{
    [ExtendObjectType(typeof(Mutation))]
    public class GalleryMutation
    {
        private readonly IGalleryServices _services;
        private string[] acceptedExt = new string[] {".jpg", ".jpeg", "png", ".gif"};
        public GalleryMutation(IGalleryServices services)
        {
            _services = services;
        }

        public async Task<GalleryDto> AddGalleryAsync(GalleryTypeInput input)
        {
            try
            {
                GalleryDto dto = new GalleryDto();
                dto.Name = input.Name;
                dto.Description = input.Description;

                string fileExt = Path.GetExtension(input.File.Name);

                if (Array.IndexOf(acceptedExt, fileExt) != -1 )
                {
                    var uniqueFileName = GetUniqueName(input.File.Name);
                    var upload = Path.Combine("Resources", "Images");
                    var filePath = Path.Combine(upload, uniqueFileName);

                    using (var fileStream = new FileStream(filePath, FileMode.Create))
                    {
                        await input.File.CopyToAsync(fileStream);
                    }

                    dto.FileLink = uniqueFileName;
                }

                return await _services.Add(dto);
            }
            catch (Exception)
            {

                throw;
            }
        }

        private string GetUniqueName(string fileName)
        {
            fileName = Path.GetFileName(fileName);
            return Path.GetFileNameWithoutExtension(fileName)
                + "_" + Guid.NewGuid().ToString().Substring(0,4)
                + Path.GetExtension(fileName);
        }
    }
}

﻿using HotChocolate.Authorization;
using Store.Domain;
using Store.Domain.Dtos;
using Store.Domain.Services;
using System.Data;

namespace Store.GraphQL.Schema.Mutations
{
    [ExtendObjectType(Name = "Mutation")]
    public class CategoryMutation
    {
        private readonly ICategoryService _service;
        public CategoryMutation(ICategoryService service)
        {
            _service = service;
        }

        [Authorize(Roles = new[] { "admin" })]
        public async Task<CategoryDto> addCategory(CategoryTypeInput input)
        {
            CategoryDto dto = new CategoryDto();
            dto.Name = input.Name;
            dto.Description = input.Description;
            var result = await _service.AddCategory(dto);

            return result;
        }

        [Authorize(Roles = new[] { "admin" })]
        public async Task<CategoryDtoUpdate> udpateCategory(CategoryTypeInput input, Guid id)
        {
            CategoryDtoUpdate dto = new CategoryDtoUpdate();
            dto.Id = id;
            dto.Name = input.Name;
            dto.Description = input.Description;
            var result = await _service.UpdateCategory(dto, id);
            if (!result)
            {
                throw new GraphQLException(new Error("Category not Found",
                    "404"));
            }
            return dto;
        }

        [Authorize(Roles = new[] { "admin" })]
        public async Task<CategoryDto> updateCategoryStatus(StoreStatusEnum status, Guid id)
        {
            var result = await _service.UpdateCategoryStatus(status, id);
            if (!result)
            {
                throw new GraphQLException(new Error("Category not found", "404"));
            }
            CategoryDto dto = await _service.GetCategoryById(id);
            return dto;
        }
    }
}

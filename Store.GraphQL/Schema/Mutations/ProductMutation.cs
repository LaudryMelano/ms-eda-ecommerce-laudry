﻿using Store.Domain.Dtos;
using Store.Domain.Services;
using Store.Domain;
using System.Data;
using HotChocolate.Authorization;

namespace Store.GraphQL.Schema.Mutations
{
    [ExtendObjectType(Name = "Mutation")]
    public class ProductMutation
    {
        private readonly IProductService _service;
        public ProductMutation(IProductService service)
        {
            _service = service;
        }

        [Authorize(Roles = new[] { "admin"})]
        public async Task<ProductDto> addProduct(ProductTypeInput input)
        {
            ProductDto dto = new ProductDto();
            dto.Name = input.Name;
            dto.CategoryId = input.CategoryId;
            dto.AttributeId = input.AttributeId;
            dto.SKU = input.SKU;
            dto.Price = input.Price;
            dto.Volume = input.Volume;
            dto.Sold = input.Sold;
            dto.Stock = input.Stock;
            dto.Description = input.Description;

            var result = await _service.AddProduct(dto);

            return result;
        }

        [Authorize(Roles = new[] { "admin" })]
        public async Task<ProductDtoUpdate> udpateProduct(ProductTypeInput input, Guid id)
        {
            ProductDtoUpdate dto = new ProductDtoUpdate();
            dto.Id = id;
            dto.Name = input.Name;
            dto.CategoryId = input.CategoryId;
            dto.AttributeId = input.AttributeId;
            dto.SKU = input.SKU;
            dto.Price = input.Price;
            dto.Volume = input.Volume;
            dto.Sold = input.Sold;
            dto.Stock = input.Stock;
            dto.Description = input.Description;
            var result = await _service.UpdateProduct(dto, id);
            if (!result)
            {
                throw new GraphQLException(new Error("Product not Found",
                    "404"));
            }
            return dto;
        }

        [Authorize(Roles = new[] { "admin" })]
        public async Task<ProductDto> updateProductStatus(StoreStatusEnum status, Guid id)
        {
            var result = await _service.UpdateProductStatus(status, id);
            if (!result)
            {
                throw new GraphQLException(new Error("Attribute not found", "404"));
            }
            ProductDto dto = await _service.GetProductById(id);
            return dto;
        }
    }
}

﻿using HotChocolate.Authorization;
using Store.Domain.Dtos;
using Store.Domain.Services;
using System.Data;

namespace Store.GraphQL.Schema.Queries
{
    [ExtendObjectType(Name = "Query")]
    public class CategoryQuery
    {
        private readonly ICategoryService _service;

        public CategoryQuery(ICategoryService service)
        {
            _service = service;
        }

        [Authorize(Roles = new[] { "admin", "customer" })]
        public async Task<IEnumerable<CategoryDto>> getAllCategory()
        {
            IEnumerable<CategoryDto> result = await _service.All();
            return result;
        }

        [Authorize(Roles = new[] { "admin", "customer" })]
        public async Task<CategoryDto> getCategoryById(Guid id)
        {
            return await _service.GetCategoryById(id);
        }
    }
}

﻿using HotChocolate.Authorization;
using Store.Domain.Dtos;
using Store.Domain.Services;
using System.Data;

namespace Store.GraphQL.Schema.Queries
{
    [ExtendObjectType(Name = "Query")]
    public class ProductQuery
    {
        private readonly IProductService _service;

        public ProductQuery(IProductService service)
        {
            _service = service;
        }

        [Authorize(Roles = new[] { "admin", "customer" })]
        public async Task<IEnumerable<ProductDto>> getAllProduct()
        {
            IEnumerable<ProductDto> result = await _service.All();
            return result;
        }

        [Authorize(Roles = new[] { "admin", "customer" })]
        public async Task<ProductDto> getProductById(Guid id)
        {
            ProductDto result = await _service.GetProductById(id);
            return result;
        }
    }
}
